using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

[RequireComponent(typeof(TextMeshProUGUI))]
public class TMP_ColorChanger : MonoBehaviour {
    public float colorChangeSpeed = 5;
    public bool modifyColor = true;

    private TextMeshProUGUI textComponent;
    private float _deltaTime;

    private void Awake() {
        textComponent = GetComponent<TextMeshProUGUI>();
    }

    private void Update() {
        if(!modifyColor)return;
        _deltaTime += Time.deltaTime * colorChangeSpeed;
        // textComponent.color = modifyColorHue(textComponent.color, _deltaTime);
    }

}