using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

[RequireComponent(typeof(AudioSource))]
public class MusicManager : MonoBehaviour {
    [System.Serializable]
    public enum eMusicType {
        cyberpunk,
        chiptune
    }

    [System.Serializable]
    public struct musicCollectionDTO {
        public eMusicType type;
        public List<AudioClip> tracks;
    }

    [System.Serializable]
    public class specificSceneMusicDTO {
        public eMusicType type;
        public AudioClip track;
        [NaughtyAttributes.Scene]
        public string sceneName;
    }

    [SerializeField]
    private eMusicType currentMusicType;
    [SerializeField]
    private float volumeFadeDuration = 1;

    public List<musicCollectionDTO> musicCollection = new List<musicCollectionDTO>();
    public List<specificSceneMusicDTO> specificSceneMusic = new List<specificSceneMusicDTO>();

    [Space(20)]
    public TextMeshProUGUI currentSongText;

    private AudioSource _ads;
    private float startingVolume;
    private static MusicManager instance;
    private void Awake() {
        instance = this;
        _ads = GetComponent<AudioSource>();
        startingVolume = _ads.volume;
    }

    private void Start() {
        PlayMusic();
    }

    public static MusicManager GetInstance() {
        return instance;
    }

    public void PlayMusic() {
        musicCollectionDTO collection = musicCollection.Find(x => x.type == currentMusicType);
        specificSceneMusicDTO specificMusic = specificSceneMusic.Find(x => x.sceneName == SceneManager.GetActiveScene().name);
        StopAllCoroutines();

        _ads.volume = 0;
        _ads.clip = (specificMusic != null) ? specificMusic.track : collection.tracks[Random.Range(0, collection.tracks.Count)];
        _ads.Play();
        _ads.DOFade(startingVolume, volumeFadeDuration);

        if(currentSongText)
            currentSongText.text = $"Current track: {_ads.clip.name}";
        StartCoroutine(SwitchTrack());
    }

    public IEnumerator SwitchTrack() {
        yield return new WaitForSeconds(_ads.clip.length);
        PlayMusic();
    }

    public void StopMusic() {
        StopAllCoroutines();
        _ads.DOFade(0, volumeFadeDuration).OnComplete(() => _ads.Stop());
    }
}