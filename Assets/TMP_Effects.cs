using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;

public class TMP_Effects : MonoBehaviour {
    [System.Serializable]
    public enum eEffect {
        Color,
        CharacterSpacing
    }

    [System.Serializable]
    public class effectOptions {
        [BoxGroup("Split/Effect", false), GUIColor(0, 1, 1), HideLabel, EnumToggleButtons]
        [HorizontalGroup("Split", 0.1f)]
        public eEffect type;

        [BoxGroup("Split/Settings"), LabelWidth(100)]
        public float changeSpeed = 1f;
        [BoxGroup("Split/Settings"), ShowIf("@type==eEffect.CharacterSpacing"), MinMaxSlider(0, 200, true), LabelWidth(100)]
        public Vector2 spacingMinMax;
        [BoxGroup("Split/Settings"), ShowIf("@type==eEffect.Color"), LabelWidth(100)]
        public bool randomStartColor;

        private float deltaTime;
        public void advanceDeltaTime(float speed) {
            deltaTime += Time.deltaTime * speed;
        }

        public float getDeltaTime() {
            return deltaTime;
        }
    }

    [SerializeField]
    private List<effectOptions> effects = new List<effectOptions>();

    private TextMeshProUGUI _textCompUI;
    private TextMeshPro _textComp;
    private void Awake() {
        _textComp = GetComponent<TextMeshPro>();
        _textCompUI = GetComponent<TextMeshProUGUI>();
    }

    private void Start() {
        effects.FindAll(e => e.type == eEffect.Color)?.ForEach(effect => {
            if (effect.randomStartColor) {
                setTextColor(getTextColor().setRandomHue());
            }
        });
    }

    private void Update() {
        effects.ForEach(effect => {
            effect.advanceDeltaTime(effect.changeSpeed);
            applyEffect(effect);
        });
    }

    private void applyEffect(effectOptions effect) {
        switch (effect.type) {
            case eEffect.Color:
                setTextColor(Helpers.modifyColorHue(getTextColor(), effect.getDeltaTime()));
                break;

            case eEffect.CharacterSpacing:
                float range = effect.spacingMinMax.y - effect.spacingMinMax.x;
                float spacing =
                    range / 2 +
                    Mathf.Abs(Mathf.Sin(effect.getDeltaTime()) * (range / 2));
                setCharacterSpacing(spacing);
                break;
        }
    }

    private void setCharacterSpacing(float spacing) {
        if (_textCompUI) {
            _textCompUI.characterSpacing = spacing;
        } else {
            _textComp.characterSpacing = spacing;
        }
    }

    private void setTextColor(Color color) {
        if (_textCompUI) {
            _textCompUI.color = color;
        } else {
            _textComp.color = color;
        }
    }

    public Color getTextColor() {
        if (_textCompUI) {
            return _textCompUI.color;
        } else {
            return _textComp.color;
        }
    }
}