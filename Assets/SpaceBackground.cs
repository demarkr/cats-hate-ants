using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Video;

public class SpaceBackground : MonoBehaviour {
    [SerializeField, Range(0, 2)]
    private float scoreSpeedMultiplier = 0.1f;

    [Space(5)]
    [SerializeField, MinMaxSlider(0f, 20, true)]
    private Vector2 speedRange = Vector2.one;
    [SerializeField, Range(0, 10)]
    private float speedNormalizeRate = 1;

    [ShowInInspector, ReadOnly]
    private float spaceMoveSpeed = 1;

    private VideoPlayer videoPlayer;

    private void Awake() {
        videoPlayer = GetComponentInChildren<VideoPlayer>();
        if (!videoPlayer)Debug.LogError("No video player found in children!");

        ScoreKeeper.onScoreAdded += scoreAdded;
        spaceMoveSpeed = speedRange.x;
    }

    private void scoreAdded(int score) {
        spaceMoveSpeed += (spaceMoveSpeed * scoreSpeedMultiplier * score);
        spaceMoveSpeed = Mathf.Clamp(spaceMoveSpeed, speedRange.x, speedRange.y);
    }

    private void Update() {
        spaceMoveSpeed = Mathf.Lerp(spaceMoveSpeed, speedRange.x, speedNormalizeRate * Time.deltaTime);
        videoPlayer.playbackSpeed = spaceMoveSpeed;
    }
}