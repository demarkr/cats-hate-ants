using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;

public class TutorialManager : MonoBehaviour {
    [System.Serializable]
    public enum eTutorialStage {
        None,
        Pat,
        Drag,
        Explode,
        AntSlap,
        AntThrow,
        Food,
        Done
    }

    [System.Serializable]
    public class Stage {
        public eTutorialStage action;
        public NotificationManager.NotificationArgs message;
        public bool completed = false;

        public eTutorialStage nextStage;
    }

    [SerializeField]
    private List<Stage> Stages = new List<Stage>();

    [SerializeField]
    private Transform tutorialCursor;
    [SerializeField]
    private float cursorMoveSpeed = 5;
    [SerializeField, BoxGroup("Drag")]
    private Transform dragWaypoint;
    [SerializeField, BoxGroup("Drag")]
    private float dragCursorMoveDuration = 1;
    [SerializeField, BoxGroup("Explode")]
    private WeaponScriptable equipWeapon;
    [SerializeField, BoxGroup("Ant")]
    private EnemyScriptable tutorialAnt;
    [SerializeField, BoxGroup("Ant")]
    private Transform antDragWaypoint;
    [SerializeField, BoxGroup("Food")]
    private GameObject foodEntity;

    [SerializeField, BoxGroup("Effects")]
    private AudioCollectionScriptable stageCompletedSound;
    [SerializeField, BoxGroup("Effects")]
    private ParticleManager.eParticleType stageCompletedParticle;

    [Title(""), NaughtyAttributes.Scene]
    public string GameScene;

    private Stage currentStage;

    // Pat
    private Transform cat;
    // Drag
    private Draggable catDraggable;
    // Explode
    private CatWeapons catWeapons;
    // Ant
    private Entity spawnedAnt;

    private void Start() {
        cat = Cat.GetInstance().transform;
        catDraggable = cat.GetComponentInChildren<Draggable>();
        catWeapons = cat.GetComponent<CatWeapons>();

        startStage(eTutorialStage.Pat);
    }

    private void startStage(eTutorialStage action) {
        Stage correspondingStage = GetStage(action);

        setUp(correspondingStage);
        NotificationManager.GetInstance().ShowNotification(correspondingStage.message);

        currentStage = correspondingStage;
    }

    private void setUp(Stage stage) {
        switch (stage.action) {
            case eTutorialStage.Pat:
                Cat.onPatEvent += onCatPat;
                break;
            case eTutorialStage.Drag:
                tutorialCursor.DOMove(dragWaypoint.position, dragCursorMoveDuration).SetLoops(-1).SetId(8);
                break;
            case eTutorialStage.Explode:
                DOTween.Restart(8);
                DOTween.Kill(8);
                catWeapons.equipWeapon(equipWeapon);
                CatWeapons.onShotWeapon += onCatExplode;
                stage.message.content = stage.message.content.Replace("{price}", equipWeapon.patCost.ToString());
                break;
            case eTutorialStage.AntSlap:
                spawnTutorialAnt(setClickable: true, setBoundsKill: false);
                break;
            case eTutorialStage.AntThrow:
                spawnTutorialAnt(setClickable: false, setBoundsKill: true);

                tutorialCursor.DOMove(antDragWaypoint.position, dragCursorMoveDuration).SetId(10).OnComplete(
                    () => restartCursorTween()
                );

                break;
            case eTutorialStage.Food:
                DOTween.Kill(10);
                tutorialCursor.position = Vector3.up * 100;
                FoodLifeManager.GetInstance().AddFood(foodEntity);
                NotificationManager.onNotificationDone += foodNotificationDone;
                break;
            case eTutorialStage.Done:
                spawnTutorialAnt(setClickable: true, setBoundsKill: false);
                break;
            default:
                break;
        }
    }

    private void spawnTutorialAnt(bool setClickable, bool setBoundsKill) {
        spawnedAnt = EnemySpawner.GetInstance().spawnEnemy(tutorialAnt)[0].GetComponent<Entity>();
        if (!setClickable)spawnedAnt.GetComponentInChildren<Clickable>().setAllowSingleClick(false);
        if (setBoundsKill)spawnedAnt.GetComponent<EntityBounds>().setBoundsAction(EntityBounds.eBoundsAction.Kill);
        spawnedAnt.OnKill += onAntKill;
    }

    public void MouseUp() {
        // Called from Unity-Event on the cat object
        if (currentStage.action == eTutorialStage.Drag) {
            catDraggable.setCanDrag(true);
        }
    }

    private void foodNotificationDone() {
        NotificationManager.onNotificationDone -= foodNotificationDone;
        completeStage(currentStage);
    }

    private void Update() {
        setCursorPosition();

    }

    private void onCatPat() {
        Cat.onPatEvent -= onCatPat;
        completeStage(currentStage);
    }

    public void onCatDrag() {
        // Called from Unity-Event on the cat object
        if (currentStage.action != eTutorialStage.Drag)return;
        completeStage(currentStage);
    }

    private void onCatExplode() {
        if (currentStage.action != eTutorialStage.Explode)return;
        CatWeapons.onShotWeapon -= onCatExplode;

        completeStage(currentStage);
    }

    private void onAntKill() {
        spawnedAnt.OnKill -= onAntKill;
        if (currentStage.action == eTutorialStage.AntSlap) {
            completeStage(currentStage);
        } else {
            completeStage(currentStage);
        }
    }

    private void restartCursorTween() {
        DOTween.Kill(10);
        tutorialCursor.position = spawnedAnt.transform.position.setZValue(tutorialCursor.position.z);
        tutorialCursor.DOMove(antDragWaypoint.position, dragCursorMoveDuration).SetId(10).OnComplete(
            () => restartCursorTween()
        );
    }

    private void setCursorPosition() {
        switch (currentStage.action) {
            case eTutorialStage.Pat:
            case eTutorialStage.Explode:
                tutorialCursor.position = cat.position.setZValue(tutorialCursor.position.z);
                break;
            case eTutorialStage.AntSlap:
                tutorialCursor.position = spawnedAnt.transform.position.setZValue(tutorialCursor.position.z);
                break;
        }
    }

    private Stage GetStage(eTutorialStage action) {
        return Stages.Find(x => x.action == action);
    }

    private void completeStage(Stage _stage) {
        Debug.Log($"Completed stage: {_stage.action.ToString()}");
        //  Effects
        SoundManager.GetInstance().PlaySound(stageCompletedSound);
        ParticleManager.GetInstance().particleAtMouse(stageCompletedParticle);

        NotificationManager.GetInstance().EndNotification();
        _stage.completed = true;
        if (_stage.nextStage != eTutorialStage.None)startStage(_stage.nextStage);
        if (_stage.action == eTutorialStage.Done)ScreenManager.getInstance().SwitchScene(GameScene);
    }
}