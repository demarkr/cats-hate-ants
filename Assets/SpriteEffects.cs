using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;

public class SpriteEffects : MonoBehaviour {

    // Appear Effects
    [SerializeField]
    private bool appearEffects = false;
    [BoxGroup("Appear Effects"), ShowIf("appearEffects"), SerializeField]
    private float appearDuration = 1f;
    [BoxGroup("Appear Effects"), ShowIf("appearEffects"), SerializeField]
    private bool randomFlashColor = false;
    [BoxGroup("Appear Effects"), ShowIf("@(this.appearEffects && !this.randomFlashColor)"), SerializeField]
    private Color appearFlashColor;
    [BoxGroup("Appear Effects"), ShowIf("appearEffects"), SerializeField]
    private float appearColorFlashDuration = 0.3f;

    [BoxGroup("Color"), SerializeField]
    private bool RandomColor = false;

    private SpriteRenderer spriteRenderer;
    private float startingAlpha;
    private Color startingColor;
    private void Awake() {
        spriteRenderer = GetComponent<SpriteRenderer>();
        startingColor = RandomColor ? Helpers.RandomColor() : spriteRenderer.color;
        startingAlpha = startingColor.a;
    }

    private void Start() {
        if (appearEffects) {
            Appear();
        }
    }

    public void Appear() {
        spriteRenderer.color = spriteRenderer.color.setAlpha(0);
        Sequence seq = DOTween.Sequence();
        seq.Append(
            spriteRenderer.DOFade(startingAlpha, appearDuration)
        );
        seq.Join(
            spriteRenderer.DOColor(randomFlashColor ? Helpers.RandomColor() : appearFlashColor, appearColorFlashDuration)
        );
        seq.Append(
            spriteRenderer.DOColor(startingColor, appearColorFlashDuration)
        );
    }

}