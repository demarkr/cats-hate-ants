using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PostProcessing;
using NaughtyAttributes;

public class GameSettings : MonoBehaviour
{
    [ShowNonSerializedField]
    public static float VIBRATION_STRENGTH = 100;
    public PostProcessingBehaviour effects;


    public void toggleEffects(){
        effects.enabled = !effects.enabled;
    }
}
