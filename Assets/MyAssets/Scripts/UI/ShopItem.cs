using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ShopItem : MonoBehaviour {
    [BoxGroup("UI References"), SerializeField, ChildGameObjectsOnly]
    private Image icon;
    [BoxGroup("UI References"), SerializeField, ChildGameObjectsOnly]
    private TextMeshProUGUI nameText;
    [BoxGroup("UI References"), SerializeField, ChildGameObjectsOnly]
    private TextMeshProUGUI price;
    [BoxGroup("UI References"), SerializeField, ChildGameObjectsOnly]
    private TextMeshProUGUI buyCounter;

    private int purchaseTimes = 0;
    private ShopItemScriptable data;
    private Button buttonRef;
    private void Awake() {
        buttonRef = GetComponent<Button>();
    }

    public void SetData(ShopItemScriptable data) {
        this.data = data;
        buttonRef.onClick.AddListener(() => ShopManager.GetInstance().BuyItem(this));
        updateUI();
    }

    public ShopItemScriptable GetData() {
        return data;
    }

    public void purchase() {
        purchaseTimes++;
        if (purchaseTimes > 0 && data.singlePurchase)buttonRef.interactable = false;

        updateUI();
    }

    private void updateUI() {
        icon.sprite = data.icon;
        nameText.text = data.name;
        price.text = $"{data.price} PATS";
        buyCounter.text = $"{purchaseTimes}/{data.maxPurchaseTimes}";
    }
}