using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;

public class Score : MonoBehaviour {
	public float moveSpeed = 1;
	public float scoreDuration = 0.5f;
	public float boundsOffset = 0.2f;

	private TextMeshPro _textMesh;
	private void Awake() {
		_textMesh = GetComponent<TextMeshPro>();
	}

	private float alpha = 1;
	void Start() {
		keepScoreInBounds();

		transform.localScale = transform.localScale * Random.Range(0.5f, 1.5f);
		transform.position = transform.position + Helpers.RandomVector2(new Vector2(-1, 1));
	}

	public void showScore() {
		alpha = 1;
		DOTween.To(() => alpha, x => alpha = x, 0, scoreDuration).OnComplete(() => Destroy(gameObject));
	}

	private void keepScoreInBounds() {
		var bounds = CameraManager.calculateBounds();
		Vector2 newPos = transform.position;

		float leftBound = bounds.bottomLeft.x + boundsOffset;
		float rightBound = bounds.topRight.x - boundsOffset;
		float topBound = bounds.topRight.y - boundsOffset;
		float bottomBound = bounds.bottomLeft.y + boundsOffset;

		transform.position = newPos.ClampVector(leftBound, rightBound, topBound, bottomBound);
	}

	private void Update() {
		transform.Translate(Vector3.up * moveSpeed * Time.deltaTime);
		_textMesh.color = new Color(_textMesh.color.r, _textMesh.color.g, _textMesh.color.b, alpha);
	}

	public void setText(string text) {
		_textMesh.text = text;
	}
}