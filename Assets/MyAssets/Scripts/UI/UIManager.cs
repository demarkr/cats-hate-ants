using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using NaughtyAttributes;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {
	public static UIManager instance;

	[SerializeField]
	private Canvas canvas;

	[SerializeField, Scene, FoldoutGroup("Game Over")]
	private string GameOverScene = "GameOver";
	[SerializeField, FoldoutGroup("Game Over")]
	private float GameOverWait = 2;

	[SerializeField, SceneObjectsOnly]
	private TextMeshProUGUI patCount;

	[SerializeField, AssetsOnly]
	private GameObject scorePrefab;

	[FoldoutGroup("Pat"), SerializeField, SceneObjectsOnly]
	private RectTransform patImage;
	[FoldoutGroup("Pat"), SerializeField]
	private string patCountFormat = "0000.##";
	[SerializeField, FoldoutGroup("Pat")]
	private string pat_animatorTrigger = "Pat";
	[SerializeField, FoldoutGroup("Pat")]
	private float pat_colorChangeSpeedMultiplier = 1.1f;

	[FoldoutGroup("Pat"), LabelWidth(130), Space(10)]
	public ShakeParametersDTO patScreenShake;
	[FoldoutGroup("Pat"), LabelWidth(130), Space(10)]
	public ShakeParametersDTO cameraSizePunch;
	[FoldoutGroup("Pat"), LabelWidth(130), Space(10)]
	public ShakeParametersDTO patIconShake;

	[FoldoutGroup("Cat Damage"), LabelWidth(150)]
	public ShakeParametersDTO catDamageScreenShake;

	[SerializeField, FoldoutGroup("Hearts")]
	private GameObject heartsPrefab;
	[SerializeField, FoldoutGroup("Hearts")]
	private Transform heartsParent;

	[FoldoutGroup("Hearts"), LabelWidth(130)]
	public ShakeParametersDTO damageHeartIconShake;

	private Animator patAnimator;
	private Cat catInstance;
	private Animator animator;

	private void Awake() {
		instance = this;
		PatManager.patChangedEvent += patCountChange;
		animator = GetComponentInChildren<Animator>();
	}

	private void Start() {
		catInstance = Cat.GetInstance();
		if (!catInstance)return;
		catInstance.OnDamage += catDamage;
		catInstance.OnKill += catDied;
	}

	private void patCountChange(int patCount) {
		this.patCount.text = patCount.ToString(patCountFormat);
	}

	private void catDamage() {
		if (getHeartsCount() > catInstance.getHealthPoints()) {
			destroyHeart();
		} else if (getHeartsCount() < catInstance.getHealthPoints()) {
			Instantiate(heartsPrefab, heartsParent);
		}
	}

	private void catDied() {
		StartCoroutine(switchScene_GameOver());
	}

	IEnumerator switchScene_GameOver() {
		yield return new WaitForSeconds(GameOverWait);
		ScoreKeeper.SaveScore();

		// Unsubscribe from events
		PatManager.patChangedEvent -= patCountChange;

		ScreenManager.getInstance().SwitchScene(GameOverScene);
	}

	private void destroyHeart() {
		Transform lastHeart = heartsParent.GetChild(catInstance.getHealthPoints());

		CameraManager.GetInstance().screenShake(catDamageScreenShake.strength, catDamageScreenShake.duration, catDamageScreenShake.vibrato);
		Sequence seq = DOTween.Sequence();
		seq.Append(
			lastHeart.DOShakePosition(
				duration: damageHeartIconShake.duration,
				strength: damageHeartIconShake.strength,
				vibrato: damageHeartIconShake.vibrato,
				fadeOut: false)
		);
		seq.Join(
			lastHeart.GetComponentInChildren<Image>()
			.DOFade(endValue: 0, duration: 0.25f)
		);
		seq.OnComplete(() => Destroy(lastHeart.gameObject));
	}

	private int getHeartsCount() {
		return heartsParent.childCount;
	}

	public void Pat() {

		// Pat UI icon shake
		DOTween.Restart(patImage);
		DOTween.Kill(patImage);
		patImage.DOShakePosition(patIconShake.duration, patIconShake.strength, patIconShake.vibrato, snapping : true);

		// Pat UI icon animation
		if (!patAnimator)patAnimator = patImage.GetComponent<Animator>();
		patAnimator.SetTrigger(pat_animatorTrigger);

		// Particle
		ParticleManager.GetInstance().particleAtMouse(ParticleManager.eParticleType.Pat);

		// Screen shake
		CameraManager.GetInstance().screenShake(patScreenShake.strength, patScreenShake.duration, patScreenShake.vibrato);

		// Change camera colors faster
		CameraManager.GetInstance().AdvanceColorMultiplier(pat_colorChangeSpeedMultiplier);

		// Zoom in camera a little
		CameraManager.GetInstance().AdvanceSizeOffset(-cameraSizePunch.strength, cameraSizePunch.duration, cameraSizePunch.vibrato);
	}

	public Vector2 WorldToCanvasPosition(Vector3 position) {
		Camera camera = Camera.main;
		RectTransform canvasRect = canvas.GetComponent<RectTransform>();
		Vector2 screenPoint = RectTransformUtility.WorldToScreenPoint(camera, position);
		Vector2 result;
		RectTransformUtility.ScreenPointToLocalPointInRectangle(
			canvasRect, screenPoint,
			canvas.renderMode == RenderMode.ScreenSpaceOverlay ? null : camera,
			out result
		);

		return canvas.transform.TransformPoint(result);
	}

	public void popupScore(PopupParametersDTO args) {
		if (args.GetPosition() == default(Vector3))
			args.SetPosition(CameraManager.GetInstance().GetWorldMousePosition());

		Score score = Instantiate(scorePrefab, args.GetPosition(), Quaternion.identity).GetComponent<Score>();
		score.setText(args.GetText());
		if (args.GetDuration() != 0)score.scoreDuration = args.GetDuration();
		if (args.GetSpeed() != 0)score.moveSpeed = args.GetSpeed();
		if (args.GetScale() != default(Vector3))score.transform.localScale = args.GetScale();
		score.showScore();
	}

	public void triggerUIAnimation(string trigger) {
		animator.SetTrigger(trigger);
	}
}