using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;

public class GameOverScoreUI : MonoBehaviour {
    [BoxGroup("UI")]
    public TextMeshProUGUI scoreTextComp;
    [BoxGroup("UI")]
    public TextMeshProUGUI highScoreTextComp;
    [BoxGroup("UI")]
    public RectTransform scoreTitlePanel;
    [BoxGroup("UI")]
    public CanvasGroup scorePanel;
    [BoxGroup("UI")]
    public CanvasGroup newHighscoreText;
    [BoxGroup("UI")]
    public CanvasGroup highScorePanel;

    [FoldoutGroup("Effect")]
    public float panel_showDuration = 1;
    [FoldoutGroup("Effect")]
    public float titlePanel_showDuration = 0.5f;
    [FoldoutGroup("Effect")]
    public float score_showDuration = 1;
    [FoldoutGroup("Effect")]
    public float highscore_yOffset = 10;

    [SerializeField]
    private string format = "0000.##";

    private Vector2 startingScoreTitlePos;
    private float highScorePanelStartY;
    private float scoreTitleY;
    private float highScorePanelY;
    private string scoreText;
    private string highScoreText;

    private void Start() {
        uiSetup();
        ScreenManager.getInstance().onDoneFade += updateScoreUI;
    }

    private void uiSetup() {
        startingScoreTitlePos = scoreTitlePanel.anchoredPosition;
        highScorePanelStartY = highScorePanel.GetComponent<RectTransform>().anchoredPosition.y;
        scoreText = "0000";
        highScoreText = "0000";
        scoreTitleY = 100;
        scorePanel.alpha = 0;
        newHighscoreText.alpha = 0;
        highScorePanel.alpha = 0;
        scoreTitlePanel.GetComponent<CanvasGroup>().alpha = 0;
    }

    public void updateScoreUI() {

        int lastScore = ScoreKeeper.GetLastSavedScore();
        int lastHighscore = ScoreKeeper.GetLastHighscore();

        Sequence seq = DOTween.Sequence();
        seq.Append(
            scorePanel.DOFade(1, panel_showDuration)
        );
        seq.Insert(panel_showDuration / 2,
            DOTween.To(() => scoreTitleY, (x) => scoreTitleY = x, startingScoreTitlePos.y, titlePanel_showDuration)
        );
        seq.Join(scoreTitlePanel.GetComponent<CanvasGroup>().DOFade(1, titlePanel_showDuration));
        seq.Append(
            DOTween.To(() => scoreText, x => scoreText = x, lastScore.ToString(format), score_showDuration).SetOptions(true, ScrambleMode.Numerals)
        );
        if (lastScore > lastHighscore) {
            lastHighscore = lastScore;
            highScoreTextComp.GetComponent<TMP_ColorChanger>().modifyColor = true;
            seq.Append(newHighscoreText.DOFade(1, 1));
            seq.Join(newHighscoreText.GetComponent<RectTransform>().DOPunchScale(new Vector3(1.5f, 1.5f, 1.5f), 1, vibrato : 4));
        }

        seq.Append(highScorePanel.DOFade(1, 1));
        seq.Join(
            DOTween.To(() => highScorePanelY, x => highScorePanelY = x, highscore_yOffset, 1.5f)
        );
        seq.Append(
            DOTween.To(() => highScoreText, x => highScoreText = x, lastHighscore.ToString(format), score_showDuration).SetOptions(true, ScrambleMode.Numerals)
        );
    }

    private void Update() {
        scoreTitlePanel.anchoredPosition = new Vector2(0, scoreTitleY);
        scoreTextComp.text = scoreText;

        highScorePanel.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, highScorePanelStartY + highscore_yOffset);
        highScoreTextComp.text = highScoreText;
    }

    private void OnDestroy() {
        ScreenManager.getInstance().onDoneFade -= updateScoreUI;
    }
}