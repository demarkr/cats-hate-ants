using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class XPManager : MonoBehaviour {
    [SerializeField, BoxGroup("References")]
    private TextMeshProUGUI levelIndicatorText;
    [SerializeField, BoxGroup("References")]
    private Image xpProgressBar;
    [SerializeField, BoxGroup("References")]
    private RectTransform barEdgeRect;
    [SerializeField, BoxGroup("References")]
    private ParticleSystem xpParticle;
    [SerializeField, BoxGroup("References")]
    private TextMeshProUGUI xpProgressText;

    [SerializeField, BoxGroup("Level Up")]
    private string levelUpAnimatorTrigger = "LevelUp";
    [SerializeField, BoxGroup("Level Up")]
    private AudioCollectionScriptable levelUpAudio;
    [SerializeField, BoxGroup("Level Up")]
    private ParticleManager.eParticleType levelUpParticle;

    [FoldoutGroup("Fetch Xp"), SerializeField]
    public bool fetchXpStats = false;
    [FoldoutGroup("Fetch Xp"), SerializeField, ShowIf("fetchXpStats"), Range(0.0f, 0.5f)]
    private float xpFetchDuration = 0.04f;

    public static int MAX_LEVEL = 100;

    private int CurrentLevel = 1;
    private int xpToLevelUp = 100;
    private int CurrentXp = 0;

    [Title("")]
    [SerializeField]
    private float xpParticlePlayTime = 0.5f;
    [SerializeField, Range(0, 10)]
    private float xpBarCatchupSpeed = 1;
    [SerializeField]
    private ShakeParametersDTO xpProgresPatShake;

    public delegate void amountChange(int amount);
    public static event amountChange onLevelUp;

    private void Awake() {
        ScoreKeeper.onScoreAdded += scoreAdded;

        CurrentXp = 0;
        xpToLevelUp = calculateNextLevelXp();
        scoreAdded(0);
    }

    private void Start() {
        if (fetchXpStats)GetXpStats();
    }

    private void scoreAdded(int score) {
        if (score > 0)SaveXpStats();
        CurrentXp += score;
        if (CurrentXp >= xpToLevelUp) {
            levelUp();
        }

        // Punch score progress text
        DOTween.Complete(124);
        if(xpProgressText){
        xpProgressText.transform.DOPunchScale(
            xpProgresPatShake.strength.vectorFromFloat(),
            xpProgresPatShake.duration,
            xpProgresPatShake.vibrato).SetId(124);
        }

        xpParticle?.Play();
        StopCoroutine(scoreParticleStop());
        StartCoroutine(scoreParticleStop());
    }

    private void levelUp() {
        onLevelUp?.Invoke(CurrentLevel);
        CurrentLevel++;
        CurrentXp = 0;
        xpToLevelUp = calculateNextLevelXp();

        // Trigger level up animation
        UIManager.instance.triggerUIAnimation("LevelUp");
        // Particle
        ParticleManager.GetInstance().particleAtPosition(levelUpParticle, Cat.GetInstance() ? Cat.GetInstance().transform.position : Vector3.zero);
        // Sound
        SoundManager.GetInstance().PlaySound(levelUpAudio);
    }

    private int calculateNextLevelXp() {
        return (CurrentLevel - 1 + CurrentLevel) * 15;
    }

    private IEnumerator scoreParticleStop() {
        yield return new WaitForSeconds(xpParticlePlayTime);
        xpParticle?.Stop();
    }

    private void Update() {
        levelIndicatorText.text = $"Level {CurrentLevel}";
        xpProgressText.text = $"{CurrentXp.ToString("000.##")}/{xpToLevelUp}";

        xpProgressBar.fillAmount = Mathf.Lerp(
            xpProgressBar.fillAmount,
            Mathf.InverseLerp(0, xpToLevelUp, CurrentXp),
            xpBarCatchupSpeed * Time.deltaTime
        );
        barEdgeRect.anchorMin = new Vector2(xpProgressBar.fillAmount, barEdgeRect.anchorMin.y);
        barEdgeRect.anchorMax = new Vector2(xpProgressBar.fillAmount, barEdgeRect.anchorMax.y);
        barEdgeRect.anchoredPosition = Vector2.zero;
    }

    public void SaveXpStats() {
        PlayerPrefs.SetInt("lvl", CurrentLevel);
        PlayerPrefs.SetInt("xp", CurrentXp);
    }

    [Button("Save Max XP")]
    private void saveMaxXp() {
        PlayerPrefs.SetInt("lvl", 100);
        PlayerPrefs.SetInt("xp", 100);
    }

    public void GetXpStats() {
        int fetchedLvl = PlayerPrefs.GetInt("lvl");
        int fetchedXp = PlayerPrefs.GetInt("xp");
        print($"Fetched lvl: {fetchedLvl} | Fetched Xp: {fetchedXp}");
        if (CurrentLevel != fetchedLvl) {
            // Current lvl is lower, Level up
            DOTween.To(() => CurrentXp, x => CurrentXp = x, xpToLevelUp, xpFetchDuration / (fetchedLvl/CurrentLevel))
                .SetEase(Ease.OutQuint).OnComplete(GetXpStats)
                .OnStepComplete(() => scoreAdded(0));
            return;
        } else if (CurrentXp != fetchedXp) {
            // Lvl matches, lerp xp
            DOTween.To(() => CurrentXp, x => CurrentXp = x, fetchedXp, xpFetchDuration).SetEase(Ease.OutQuint)
                .OnStepComplete(() => scoreAdded(0));
        }
    }

    private void OnDestroy() {
        ScoreKeeper.onScoreChanged -= scoreAdded;
    }

    private void OnDisable() {
        ScoreKeeper.onScoreChanged -= scoreAdded;
    }
}