using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

public class ShopManager : MonoBehaviour {
    [BoxGroup("References"), ChildGameObjectsOnly]
    public Transform ShopItemsPanel;
    [BoxGroup("References"), AssetsOnly]
    public GameObject ShopItemPrefab;

    [BoxGroup("Extras")]
    public AudioCollectionScriptable BuySound;

    [AssetList(Path = "MyAssets/Scriptables/Shop Items", AutoPopulate = true)]
    public List<ShopItemScriptable> ShopItems = new List<ShopItemScriptable>();

    private static ShopManager instance;
    private void Awake() {
        instance = this;
    }

    public static ShopManager GetInstance() {
        return instance;
    }

    private void Start() {
        updateShopItems();
    }

    public void BuyItem(ShopItem item) {
        int price = item.GetData().price;
        if (PatManager.instance.getPatCount() >= price) {
            PatManager.instance.removePat(price);
            item.purchase();

            SoundManager.GetInstance().PlaySound(BuySound);
        }
    }

    private void updateShopItems() {
        ShopItemsPanel.DestroyAllChildren();
        ShopItems.ForEach(item => {
            ShopItem shopItem = Instantiate(ShopItemPrefab, ShopItemsPanel).GetComponent<ShopItem>();
            shopItem.SetData(item);
        });
    }
}