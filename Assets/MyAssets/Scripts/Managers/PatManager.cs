using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

public class PatManager : MonoBehaviour {
	[ShowInInspector, EnableGUI]
	private int PatCount = 0;

	public AudioCollectionScriptable catSound;

	public static PatManager instance;

	public delegate void PatChanged(int patCount);
	public static event PatChanged patChangedEvent;

	private UIManager _uiManager;
	private void Awake() {
		if (!instance)instance = this;
		_uiManager = GetComponent<UIManager>();
	}

	private void Start() {
		patChangedEvent?.Invoke(PatCount);
	}

	public void addPat(int amount = 1, PopupParametersDTO args = null) {
		if (amount == 0)return;

		// Play sound
		SoundManager.GetInstance().PlaySound(catSound);

		// Add Score
		ScoreKeeper.AddScore(amount);

		PatCount += amount;
		_uiManager.Pat();
		patChangedEvent?.Invoke(PatCount);

		// Popup score
		if (args == null)args = new PopupParametersDTO();
		args.SetText($"+{amount} XP");
		UIManager.instance.popupScore(args);

		// Particle
		ParticleManager.GetInstance().particleAtPosition(ParticleManager.eParticleType.PatPop, args.GetPosition());
	}

	public void removePat(int amount = 1, PopupParametersDTO args = null) {
		if (amount == 0)return;

		PatCount -= amount;
		patChangedEvent?.Invoke(PatCount);

		// Popup score
		if (args == null)args = new PopupParametersDTO(position: CameraManager.GetInstance().GetWorldMousePosition());
		args.SetText($"-{amount}");
		UIManager.instance.popupScore(args);
	}

	public int getPatCount() {
		return PatCount;
	}
}