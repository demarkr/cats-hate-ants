using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ScreenManager : MonoBehaviour {
    [SerializeField]
    private Image BlackScreen;
    [SerializeField, FoldoutGroup("Fade Settings")]
    private float fadeUpSpeed = 0.5f;
    [SerializeField, FoldoutGroup("Fade Settings")]
    private float fadeDownSpeed = 0.5f;

    public int MAX_FPS = 60;

    public delegate void doneFadeDelegate();
    public event doneFadeDelegate onDoneFade;

    private static ScreenManager instance;

    private void Awake() {
        instance = this;
        Application.targetFrameRate = MAX_FPS;
        BlackScreen.color = new Color(0, 0, 0, 1);
    }

    private void Start() {
        fadeDown();
    }

    public void SwitchScene(string sceneName) {
        BlackScreen.raycastTarget = true;

        MusicManager.GetInstance().StopMusic();
        BlackScreen.DOFade(1, fadeUpSpeed).OnComplete(() => {
            AsyncOperation newScene = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Single);
            newScene.completed += sceneLoaded;
        });
    }

    public void Quit() {
        Application.Quit();
    }

    private void sceneLoaded(AsyncOperation obj) {
        if (obj.isDone) {
            fadeDown();
        }
    }

    private void fadeDown() {
        BlackScreen.DOFade(0, fadeDownSpeed).OnComplete(() => BlackScreen.raycastTarget = false).OnComplete(() => onDoneFade?.Invoke());
        MusicManager.GetInstance().PlayMusic();
    }

    public static ScreenManager getInstance() {
        return instance;
    }
}