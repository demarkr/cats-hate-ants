using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using NaughtyAttributes;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;
using ReadOnlyAttribute = Sirenix.OdinInspector.ReadOnlyAttribute;

[System.Serializable]
public class FoodLifeManager : MonoBehaviour {
    public class FoodEntityParameters {
        public Entity entity;
        public float HealthBarUpdateSpeed = 5;

        public FoodEntityParameters(Entity entity) {
            this.entity = entity;
            this.HealthBarUpdateSpeed = 5;
            this.initialized = false;
        }

        [ReadOnly]
        public bool initialized = false;
        [ReadOnly]
        public RectTransform healthUI_Transform;
        [ReadOnly]
        public CanvasGroup healthUI;
        [ReadOnly]
        public Image healthBar;
        [ReadOnly]
        public int startLife;
    }

    [SerializeField]
    private GameObject FoodHealthUiPrefab;
    [Tag, SerializeField]
    private string healthBarTag;
    [SerializeField]
    private Canvas mainCanvas;

    [SerializeField, Title("")]
    private List<FoodEntityParameters> foodEntities = new List<FoodEntityParameters>();

    [FoldoutGroup("Health UI Effects")]
    public float healthEffects_AppearDur = 1;
    [FoldoutGroup("Health UI Effects")]
    public float healthEffects_AppearScaleMult = 1.1f;
    [FoldoutGroup("Health UI Effects")]
    public float healthEffects_AppearScaleDur = 0.5f;

    public float damageHueChangeMultiplier = 2;
    private static FoodLifeManager instance;

    public static FoodLifeManager GetInstance() {
        return instance;
    }

    private void Awake() {
        instance = this;
    }

    private void Start() {
        initializeFoodEntityUI();
    }

    private void initializeFoodEntityUI() {
        foodEntities.FindAll(param => !param.initialized).ForEach(entityParameters => {
            entityParameters.healthUI = Instantiate(FoodHealthUiPrefab, mainCanvas.transform).GetComponent<CanvasGroup>();

            // Appear effects
            // Fade
            entityParameters.healthUI.alpha = 0;
            entityParameters.healthUI.DOFade(1, healthEffects_AppearDur).SetEase(Ease.OutQuint);

            // Punch
            entityParameters.healthUI.transform.DOPunchScale(
                entityParameters.healthUI.transform.localScale * healthEffects_AppearScaleMult,
                healthEffects_AppearScaleDur, 6).SetEase(Ease.OutQuint);

            // Particles
            ParticleManager.GetInstance().particleAtPosition(ParticleManager.eParticleType.ConfettiBirthday, entityParameters.entity.transform.position);

            entityParameters.healthUI_Transform = entityParameters.healthUI.GetComponent<RectTransform>();
            entityParameters.healthBar = entityParameters.healthUI.transform.FindComponentInChildWithTag<Image>(healthBarTag);

            entityParameters.startLife = entityParameters.entity.getHealthPoints();
            entityParameters.entity.OnKill += foodDestroyed;
            entityParameters.entity.OnDamage += foodDamage;

            entityParameters.initialized = true;
        });
    }

    private void Update() {
        if (foodEntities.Count == 0)return;

        foodEntities.ForEach(entity => {
            float healthFillAmount = Mathf.InverseLerp(0, entity.startLife, entity.entity.getHealthPoints());
            entity.healthBar.fillAmount =
                Mathf.Lerp(entity.healthBar.fillAmount, healthFillAmount, entity.HealthBarUpdateSpeed * Time.deltaTime);

            updateSoupUIPosition(entity);
        });
    }

    public void AddFood(GameObject foodPrefab) {
        Transform spawnPosition = GameObject.Find(foodPrefab.name)?.transform;
        if (!spawnPosition) {
            Debug.LogError($"No scene food entity found with the name of {foodPrefab.name}");
            return;
        }

        Entity sceneEntity = Instantiate(foodPrefab, spawnPosition.position, spawnPosition.rotation).GetComponent<Entity>();

        foodEntities.Add(new FoodEntityParameters(sceneEntity));
        initializeFoodEntityUI();
    }

    private void updateSoupUIPosition(FoodEntityParameters entityParameters) {
        if (!entityParameters.entity)return;
        //Update soup position to world pos
        Vector2 screenPos = CameraManager.GetInstance().worldToViewpoint(entityParameters.entity.transform.position);
        entityParameters.healthUI_Transform.anchorMin = screenPos;
        entityParameters.healthUI_Transform.anchorMax = screenPos;

        // Set z to 1000 so it will be behind the food 3D model
        entityParameters.healthUI_Transform.transform.localPosition = entityParameters.healthUI_Transform.transform.localPosition.setZValue(1000);
    }

    private void foodDamage() {
        CameraManager.GetInstance().AdvanceColorMultiplier(damageHueChangeMultiplier);
    }

    private void foodDestroyed() {
        print("TOOD: SOUP DESTROYED!");
        GameObject.FindObjectOfType<Cat>()?.Kill();
    }

}