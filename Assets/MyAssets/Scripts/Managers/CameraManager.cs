using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.PostProcessing;

public class CameraManager : MonoBehaviour {
	private static CameraManager instance;

	[SerializeField, MinMaxSlider(0, 30)]
	private Vector2 colorChangeSpeedRange = Vector2.one;
	[SerializeField()]
	private float sizeChangeSpeed = 1;
	[SerializeField(), ReadOnly]
	private Color calculatedColor;
	[SerializeField(), MinMaxSlider(2, 15)]
	private Vector2 cameraScaleRange;

	[FoldoutGroup("Speed Multiplier"), SerializeField]
	private float color_MultiplierNormalizeSpeed = 2;
	[FoldoutGroup("Speed Multiplier"), SerializeField, ReadOnly]
	private float colorChangeSpeed;

	[FoldoutGroup("Speed Multiplier"), SerializeField]
	private float cameraSizeNormalizeRate = 10;
	[FoldoutGroup("Speed Multiplier"), SerializeField, ReadOnly]
	private float cameraSizeOffset = 0;

	private Camera _camera;
	private void Awake() {
		_camera = GetComponent<Camera>();
		instance = this;

		colorChangeSpeed = colorChangeSpeedRange.x;
	}

	private float _deltaTime = 0;
	private float _deltaTimeColor = 0;
	private void Update() {
		_deltaTime += Time.deltaTime;
		_deltaTimeColor += Time.deltaTime * colorChangeSpeed;

		calculatedColor = modifyColorHue(_camera.backgroundColor, _deltaTimeColor);
		_camera.backgroundColor = calculatedColor;

		normalizeColorChangeSpeed();

		modifyOrthographicSize(_deltaTime * sizeChangeSpeed);
	}

	private void normalizeColorChangeSpeed() {
		colorChangeSpeed = Mathf.Lerp(colorChangeSpeed, 0, color_MultiplierNormalizeSpeed * Time.deltaTime);
	}

	private Color modifyColorHue(Color color, float time) {
		float h, s, v;
		Color.RGBToHSV(color, out h, out s, out v);
		h = time % 1;
		return Color.HSVToRGB(h, s, v);
	}

	private void modifyOrthographicSize(float time) {
		_camera.orthographicSize = cameraSizeOffset + cameraScaleRange.x + (1 + Mathf.Sin(time / 2) * (cameraScaleRange.y - cameraScaleRange.x)) / 2;
	}

	public static(Vector2 bottomLeft, Vector2 topRight)calculateBounds() {
		Vector2 bottomLeft = Camera.main.ScreenToWorldPoint(Camera.main.ViewportToScreenPoint(Vector2.zero));
		Vector2 topRight = Camera.main.ScreenToWorldPoint(Camera.main.ViewportToScreenPoint(Vector2.one));
		return (
			bottomLeft: bottomLeft,
			topRight: topRight
		);
	}

	public static(Vector2 bottomLeft, Vector2 topRight)calculateBounds(out float width, out float height) {
		Vector2 bottomLeft = Camera.main.ScreenToWorldPoint(Camera.main.ViewportToScreenPoint(Vector2.zero));
		Vector2 topRight = Camera.main.ScreenToWorldPoint(Camera.main.ViewportToScreenPoint(Vector2.one));

		width = Mathf.Abs(topRight.x - bottomLeft.x);
		height = Mathf.Abs(topRight.y - bottomLeft.y);

		return (
			bottomLeft: bottomLeft,
			topRight: topRight
		);
	}

	public void screenShake(float strength, float duration, int vibrato = 10) {
		DOTween.Complete(1);
		transform.DOShakePosition(duration, strength, vibrato : vibrato).SetId(1);
		transform.DOShakeRotation(duration, strength, vibrato : vibrato).SetId(1);

		Vibration.Vibrate((long)(strength * GameSettings.VIBRATION_STRENGTH));
	}

	public Vector3 GetWorldMousePosition() {
		return _camera.ScreenToWorldPoint(Input.mousePosition).setZValue(0);
	}

	public Vector3 worldToViewpoint(Vector3 worldPosition) {
		return _camera.WorldToViewportPoint(worldPosition);
	}

	public Vector3 viewportToWorldPoint(Vector3 screenPos){
		return _camera.ViewportToWorldPoint(screenPos);
	}

	public static CameraManager GetInstance() {
		return instance;
	}

	public void AdvanceColorMultiplier(float multiplier = 1) {
		colorChangeSpeed += Time.deltaTime * multiplier;
		colorChangeSpeed = Mathf.Clamp(colorChangeSpeed, colorChangeSpeedRange.x, colorChangeSpeedRange.y);
	}

	private Tween sizeTween;
	public void AdvanceSizeOffset(float endOffset = 1, float duration = 0.1f, int vibrato = 5) {
		sizeTween.Complete();
		sizeTween = DOTween.Punch(
			() => vectorFromFloat(cameraSizeOffset),
			(x) => cameraSizeOffset = x.x,
			vectorFromFloat(endOffset),
			duration,
			vibrato
		);
	}

	private Vector3 vectorFromFloat(float value) {
		return new Vector3(value, value, value);
	}
}