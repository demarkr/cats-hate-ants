using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

public class ScoreKeeper : MonoBehaviour {
    [ShowInInspector(), ReadOnly()]
    private static int Score;
    [ShowInInspector, ReadOnly()]
    public static int Max_Score = 2000;

    public bool KeepScore = true;

    public delegate void scoreChangedDelegate(int score);
    public static event scoreChangedDelegate onScoreChanged;
    public static event scoreChangedDelegate onScoreAdded;

    private static ScoreKeeper instance;
    private void Start() {
        ResetScore();
        instance = this;
    }

    public static ScoreKeeper GetInstance() {
        return instance;
    }

    public static int GetScore() {
        return Score;
    }

    public static void ResetScore() {
        Score = 0;
        onScoreChanged?.Invoke(Score);
    }

    public static void AddScore(int amount) {
        if (!ScoreKeeper.GetInstance().KeepScore)return;
        Score += amount;
        onScoreChanged?.Invoke(Score);
        onScoreAdded?.Invoke(amount);

        if (Score > GetLastHighscore()) {
            SaveHighscore(Score);
        }
    }

    public static void SaveScore() {
        PlayerPrefs.SetInt("scr", Score);
    }

    public static void SaveHighscore(int score) {
        PlayerPrefs.SetInt("hscr", score);
    }

    public static int GetLastSavedScore() {
        int savedScore = PlayerPrefs.GetInt("scr", 0);
        onScoreChanged?.Invoke(savedScore);
        return savedScore;
    }

    public static int GetLastHighscore() {
        return PlayerPrefs.GetInt("hscr", 0);
    }

    [Button("Reset Highscore")]
    public void resetHighscore() {
        SaveHighscore(0);
    }

    [Button("Reset Score")]
    public void resetHighscoreButt() {
        ResetScore();
        SaveScore();
    }
}