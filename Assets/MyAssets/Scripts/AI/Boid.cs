using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using Sirenix.OdinInspector;
using UnityEngine;
using BoxGroupAttribute = Sirenix.OdinInspector.BoxGroupAttribute;

public class Boid : MonoBehaviour {
    [SerializeField, BoxGroup ("Movement")]
    private float movementSpeed;
    [SerializeField, BoxGroup ("Movement")]
    private float detectRadius = 3;

    [SerializeField, BoxGroup ("Awareness")]
    private LayerMask otherBoids;
    [SerializeField, BoxGroup ("Awareness")]
    private LayerMask obstaclesMask;
    [SerializeField, BoxGroup ("Awareness")]
    private int detectionRayCount = 4;

    [BoxGroup ("Control"), SerializeField]
    private float maxAlignForce = 1;
    [BoxGroup ("Control"), SerializeField]
    private float maxSeperationForce = 0.3f;
    [BoxGroup ("Control"), SerializeField]
    private float targetFollowForce = 0.2f;
    [BoxGroup ("Control"), SerializeField]
    private float obstacleAvoidanceMaxForce = 0.2f;
    [BoxGroup ("Control"), SerializeField]
    private float obstacleAvoidanceThreshold = 0.3f;

    [BoxGroup ("Target"), Tag, SerializeField]
    private string targetTag;
    [SerializeField, NaughtyAttributes.ReadOnly, BoxGroup ("Target")]
    private Transform target;
    [SerializeField, BoxGroup ("Target")]
    private float targetUpdateLoopInterval = 0.35f;

    [BoxGroup ("External Forces"), SerializeField, Range (0, 5)]
    private float friction = 1;

    private Vector2 velocity;
    private Vector2 acceleration = new Vector2 ();
    private Vector2 externalForce = new Vector2 ();
    private Vector2 spawnPosition;
    private bool moveable = true;
    private Rigidbody _rigidbody;

    private List<Boid> nearbyBoids;
    private List<Vector3> obstacles;
    private void Awake () {
        nearbyBoids = new List<Boid> ();
        obstacles = new List<Vector3> ();
        _rigidbody = GetComponent<Rigidbody>();
    }
    private void Start () {
        moveable=true;
        this.velocity = Random.onUnitSphere;
        StartCoroutine(targetUpdateLoop());
        spawnPosition = transform.position;
    }

    private IEnumerator targetUpdateLoop(){
        updateTarget();
        yield return new WaitForSeconds(targetUpdateLoopInterval);
        StartCoroutine(targetUpdateLoop());
    }

    private void updateTarget () {
        GameObject[] targets = GameObject.FindGameObjectsWithTag (targetTag);

        float closestDistance = Mathf.Infinity;
        Transform closestTarget = transform;
        foreach (var _target in targets) {
            float distance = Vector2.Distance (_target.transform.position, transform.position);
            if (distance < closestDistance) {
                closestDistance = distance;
                closestTarget = _target.transform;
            }
        }

        target = closestTarget;
    }

    private void FixedUpdate () {
        getNearbyBoids ();
        getObstacles ();
        getAccelerationDir ();

        transform.rotation = Quaternion.LookRotation (velocity, transform.up);

        velocity += acceleration;
        velocity.Normalize ();
        //transform.Translate ((velocity + externalForce) * Time.deltaTime * movementSpeed, Space.World);
        if(moveable)
            _rigidbody.velocity = (velocity + externalForce)*movementSpeed;

        acceleration = Vector2.zero;
        externalForce = applyFriction (externalForce);
    }

    private Vector2 applyFriction (Vector2 force) {
        return Vector3.Lerp (force, Vector3.zero, friction * Time.deltaTime);
    }

    private void getAccelerationDir () {
        var overallSteering = calculateOverallSteering (nearbyBoids);
        acceleration += overallSteering.steering + overallSteering.avoidance + overallSteering.seperation;
    }

    public void swapRigidbodyVelocityForExternalForce(){
        externalForce = _rigidbody.velocity;
    }

    public void setMoveable(bool state){
        if(state)swapRigidbodyVelocityForExternalForce();
        moveable = state;
    }

    private (Vector2 steering, Vector2 seperation, Vector2 avoidance) calculateOverallSteering (List<Boid> boids) {
        Vector2 overallSteering = new Vector2 ();
        Vector2 avoidance = new Vector2 ();
        Vector2 seperation = new Vector2 ();

        overallSteering += calculateDirToTarget () * targetFollowForce;

        boids.ForEach (boid => {
            float distance = Vector2.Distance (transform.position, boid.transform.position);

            // Velocity for alignment, other's position for cohesion, dir to target for follow
            overallSteering +=
                boid.GetVelocity () +
                boid.GetPosition ();

            seperation += (GetPosition () - boid.GetPosition ()) / distance;
        });

        obstacles.ForEach (obstacle => {
            float distance = Vector2.Distance (transform.position, obstacle);
            Vector2 directionToObstacle = (Vector2) obstacle - GetPosition ();
            float angleToObstacle = Vector2.Dot (directionToObstacle.normalized, velocity.normalized);
            if (angleToObstacle > obstacleAvoidanceThreshold) {
                avoidance += (velocity.normalized - directionToObstacle.normalized) / distance;
            }
        });

        if (boids.Count > 0) {
            overallSteering /= boids.Count;
            overallSteering -= velocity;

            seperation /= boids.Count;
            seperation -= velocity;
        }

        if (obstacles.Count > 0) {
            avoidance /= obstacles.Count;
        }

        return (
            steering: Vector2.ClampMagnitude (overallSteering - GetPosition (), maxAlignForce),
            seperation : Vector2.ClampMagnitude (seperation, maxSeperationForce),
            avoidance : Vector2.ClampMagnitude (avoidance, obstacleAvoidanceMaxForce)
        );
    }

    public void AddForce (Vector2 force) {
        externalForce += force;
    }

    public void ResetExternalForce () {
        externalForce = Vector2.zero;
    }

    public Vector2 calculateDirToTarget () {
        if (!target) return spawnPosition - (Vector2) transform.position;
        return (Vector2) target.position - (Vector2) transform.position;
    }

    private void getNearbyBoids () {
        nearbyBoids.Clear ();
        foreach (var collider in Physics.OverlapSphere (transform.position, detectRadius, otherBoids)) {
            Boid colliderBoid = collider.GetComponent<Boid> ();
            if (colliderBoid == this) continue;
            nearbyBoids.Add (colliderBoid);
        }
    }

    private void getObstacles () {
        obstacles.Clear ();

        getDetectionRays ().ForEach (ray => {
            RaycastHit hit;
            if (Physics.Raycast (ray, out hit, detectRadius, obstaclesMask)) {
                obstacles.Add (hit.point);
            }
        });
    }

    private List<Ray> getDetectionRays () {
        List<Ray> detectionRays = new List<Ray> ();
        for (var i = 0; i < detectionRayCount; i++) {
            float forwardDegress = Mathf.Atan2 (transform.right.y, transform.right.x);
            float angle = forwardDegress + (360 / detectionRayCount * i * Mathf.Deg2Rad);
            detectionRays.Add (new Ray (
                transform.position,
                new Vector2 (Mathf.Cos (angle), Mathf.Sin (angle))
            ));
        }

        return detectionRays;
    }

    private void OnDrawGizmosSelected () {
        Gizmos.color = new Color (0, 0, 0.8f, 0.2f);
        Gizmos.DrawSphere (transform.position, detectRadius);
    }

    private void OnDrawGizmos () {
        Gizmos.color = new Color (0, 1, 0, 0.25f);
        getDetectionRays ().ForEach (ray => Gizmos.DrawRay (ray));

        Gizmos.color = new Color (0, 1, 0, 1);
        Gizmos.DrawRay (transform.position, velocity);

        Gizmos.color = new Color (1, 0, 0, 1);
        Gizmos.DrawRay (transform.position, calculateDirToTarget ());
    }

    public Vector2 GetVelocity () {
        return velocity;
    }

    public Vector2 GetPosition () {
        return new Vector2 (transform.position.x, transform.position.y);
    }
}