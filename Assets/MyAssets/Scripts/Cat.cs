using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using HighlightPlus;
using Sirenix.OdinInspector;
using UnityEngine;

[RequireComponent(typeof(EntityBounds))]
public class Cat : Entity {
	[LabelWidth(100)]
	public ShakeParametersDTO punchShake = new ShakeParametersDTO(0.1f, 1.3f);

	[LabelWidth(100)]
	public ShakeParametersDTO damageShake = new ShakeParametersDTO(0.1f, 2, 2);

	public static bool CatDead = false;
	private static Cat instance;

	private float deltaTime;

	// SHAKE //
	private float shakeMultiplier = 0f;
	private float shakeTimeScale = 0f;

	// These values won't be changed
	private ShakeParametersDTO baseShake = new ShakeParametersDTO(duration: 1, strength: 0.1f);

	public delegate void patEventDelegate();
	public static event patEventDelegate onPatEvent;

	private HighlightEffect _catHighlight;
	Vector3 shakePosition;
	Tweener shakeTween;

	public void setShakeMultiplier(float value) {
		shakeMultiplier = value;
	}

	public void setShakeTimescale(float value) {
		shakeTimeScale = value;
	}

	private void Awake() {
		CatDead = false;
		instance = this;
		_catHighlight = GetComponentInChildren<HighlightEffect>();
	}

	private void Start() {
		shakeTween = DOTween.Shake(
				() => shakePosition,
				x => shakePosition = x,
				baseShake.duration, baseShake.strength, baseShake.vibrato, fadeOut : true)
			.SetLoops(-1, LoopType.Restart);
	}

	public override void Kill() {
		CatDead = true;
		base.Kill();
	}

	public override void Damage(int amount = 1) {
		base.Damage(amount);
		CameraManager.GetInstance().screenShake(damageShake.strength, damageShake.duration, damageShake.vibrato);
	}

	public void OnPat() {
		onPatEvent?.Invoke();
		doPunch();
		_catHighlight.HitFX(_catHighlight.hitFxColor, _catHighlight.hitFxFadeOutDuration, 0.25f);

		PatManager.instance.addPat();
	}

	protected override void Update() {
		base.Update();
		deltaTime += Time.deltaTime;

		transform.localPosition = transform.localPosition + shakePosition * shakeMultiplier;
		shakeTween.timeScale = shakeTimeScale;
	}

	private void doPunch() {
		DOTween.Complete(2);
		transform.DOShakeScale(punchShake.duration, punchShake.strength, punchShake.vibrato).SetId(2);
	}

	private Vector3 vectorFromFloat(float value) {
		return new Vector3(value, value, value);
	}

	public static Cat GetInstance() {
		return instance;
	}
}