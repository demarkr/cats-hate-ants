using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleManager : MonoBehaviour {
    [System.Serializable]
    public enum eParticleType {
        Explosion,
        Confetti,
        Pat,
        PatPop,
        ConfettiBirthday
    }

    [System.Serializable]
    public struct ParticleInfoStruct {
        public eParticleType type;
        public GameObject prefab;
    }

    [SerializeField]
    private List<ParticleInfoStruct> particles = new List<ParticleInfoStruct>();

    private static ParticleManager instance;
    private void Awake() {
        instance = this;
    }

    public static ParticleManager GetInstance() {
        return instance;
    }

    public void particleAtMouse(eParticleType type) {
        particleAtPosition(type, CameraManager.GetInstance().GetWorldMousePosition());
    }

    public void particleAtPosition(eParticleType type, Vector3 position) {
        GameObject prefab = particles.Find(x => x.type == type).prefab;
        if (!prefab) { Debug.LogError($"No particle prefab found for type {type}"); return; }
        Instantiate(prefab, position, Quaternion.identity, transform);
    }
}