using System;
using UnityEngine;

public class Enemy_Bird : Entity {

	[SerializeField]
	private float knockBackForce = 2;

	private DealDamage _dealDamage;
	private Boid _boid;
	private EntityBounds _entityBounds;

	private void Awake () {
		_dealDamage = GetComponent<DealDamage> ();
		_boid = GetComponent<Boid>();
		_entityBounds = GetComponent<EntityBounds> ();

		_dealDamage.DealtDamageEvent += OnDealtDamage;
	}

	private void OnDealtDamage () {
		_boid.AddForce(-_boid.calculateDirToTarget() * knockBackForce);
	}

	void OnDestroy () {
		_dealDamage.DealtDamageEvent -= OnDealtDamage;
	}
}