using UnityEngine;
using Sirenix.OdinInspector;

[CreateAssetMenu(fileName = "NewEnemy", menuName = "New Enemy")]
public class EnemyScriptable : ScriptableObject {
		[System.Flags, System.Serializable]
		public enum eBoundType {
			top = 1 << 1,
			bottom = 1 << 2,
		left = 1 << 3,
			right = 1 << 4,
			all = top | bottom | left | right
		}
		public bool spawn = true;

		[HorizontalGroup("Split", 0.34f)]
		[AssetSelector (Paths = "Assets/MyAssets/Prefabs/Enemies"), PreviewField(128), HideLabel]
		[BoxGroup("Split/Left")]
		public GameObject prefab;
		[BoxGroup("Split/Right")]
		public float minFrequency;
		[BoxGroup("Split/Right")]
		public int prespawnCount = 0;
		[Min(1)]
		[BoxGroup("Split/Right")]
		public int groupSpawnAmount = 1;
		[BoxGroup("Split/Right")]
		public int spawnAmount = 1;
		[BoxGroup("Split/Right")]
		[MinMaxSlider (0, 8)]
		public Vector2 spawnFrequency;
		[BoxGroup("Split/Right")]
		[Range (1, 30)]
		public float spawnRadius = 25;
		[BoxGroup("Split/Right")]
		[Range (-30, 30)]
		public float spawnOffset = 0;

		[FoldoutGroup ("Spawn Bounds"), SerializeField]
		public eBoundType bounds;

		[HideInInspector]
		public float nextSpawn = 0;

		[HideInInspector]
		public bool showBounds = false;
		[Button("Toggle Bounds")]
		private void toggleBounds(){
			showBounds = !showBounds;
		}
}