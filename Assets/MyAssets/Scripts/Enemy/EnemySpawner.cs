using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {
	[SerializeField]
	private List<EnemyScriptable> enemySpawns = new List<EnemyScriptable>();

	private List<EnemyScriptable> enemySpawnsRuntime = new List<EnemyScriptable>();

	private static EnemySpawner instance;

	private void Awake() {
		instance = this;

		enemySpawns.ForEach(enemy => {
			enemySpawnsRuntime.Add(cloneEnemy(enemy));
		});
	}

	private EnemyScriptable cloneEnemy(EnemyScriptable enemy) {
		EnemyScriptable clone = Object.Instantiate<EnemyScriptable>(enemy);
		clone.name = enemy.name;
		clone.nextSpawn = 0;

		return clone;
	}

	public static EnemySpawner GetInstance() {
		return instance;
	}
	private void Start() {
		preSpawn();
	}

	private float _deltaTime;
	private void Update() {
		_deltaTime += Time.deltaTime;

		enemySpawnsRuntime.ForEach(parameters => {
			if (parameters.spawn && _deltaTime > parameters.nextSpawn && !Cat.CatDead) {
				parameters.nextSpawn =
					_deltaTime +
					parameters.minFrequency +
					Random.Range(parameters.spawnFrequency.x, parameters.spawnFrequency.y);
				for (var i = 0; i < parameters.groupSpawnAmount; i++) {
					spawnEnemy(parameters);
				}
			}
		});
	}

	private void preSpawn() {
		enemySpawnsRuntime.FindAll(x => x.spawn).ForEach(parameter => {
			for (int i = 0; i < parameter.prespawnCount; i++) {
				spawnEnemy(parameter);
			}
		});
	}

	public void setSpawnState(EnemyScriptable enemy, bool state) {
		enemySpawnsRuntime.Find(x => x.name.Equals(enemy.name)).spawn = state;
	}

	public void setMinSpawnFreq(EnemyScriptable enemy, float value) {
		enemySpawnsRuntime.Find(x => x.name == enemy.name).minFrequency = value;
	}

	public void setSpawnFreq(EnemyScriptable enemy, Vector2 value) {
		enemySpawnsRuntime.Find(x => x.name == enemy.name).spawnFrequency = value;
	}

	public void setBounds(EnemyScriptable enemy, EnemyScriptable.eBoundType bounds) {
		enemySpawnsRuntime.Find(x => x.name == enemy.name).bounds = bounds;
	}

	public void setGroupSpawnAmount(EnemyScriptable enemy, int groupSpawnAmount) {
		enemySpawnsRuntime.Find(x => x.name == enemy.name).groupSpawnAmount = groupSpawnAmount;
	}

	public void resetRuntimeEnemyParams(EnemyScriptable enemy) {
		var runtimeEnemy = enemySpawnsRuntime.Find(x => x.name == enemy.name);
		enemySpawnsRuntime.Remove(runtimeEnemy);

		var clonedEnemy = cloneEnemy(enemy);
		clonedEnemy.spawn = runtimeEnemy.spawn;
		enemySpawnsRuntime.Add(clonedEnemy);
	}

	public GameObject[] spawnEnemy(EnemyScriptable enemy) {
		Vector3 spawnLoc = calulateSpawnLocation(enemy);
		GameObject[] spawned = new GameObject[enemy.spawnAmount];
		for (var i = 0; i < enemy.spawnAmount; i++) {
			spawned[i] = Instantiate(enemy.prefab, spawnLoc, calculateSpawnRotation(spawnLoc));
		}

		return spawned;
	}

	// BEWARE: Terrible code lays ahead! You have been warned :(
	private Vector2 calulateSpawnLocation(EnemyScriptable parameters) {
		(Vector2 bottomLeft, Vector2 topRight)bounds = CameraManager.calculateBounds();
		Vector2 spawnLocation = new Vector2();

		spawnLocation.x = Random.Range(bounds.bottomLeft.x, bounds.topRight.x);
		spawnLocation.y = Random.Range(bounds.bottomLeft.y, bounds.topRight.y);

		if (parameters.bounds.HasFlag(EnemyScriptable.eBoundType.right) && parameters.bounds.HasFlag(EnemyScriptable.eBoundType.left)) {
			bool spawnLeft = randomBool();
			if (spawnLeft) {
				// Spawn enemy in left side
				spawnLocation.x = calculateHorizontalSpawn(EnemyScriptable.eBoundType.left, parameters, bounds);
			} else {
				// Spawn enemy in right side
				spawnLocation.x = calculateHorizontalSpawn(EnemyScriptable.eBoundType.right, parameters, bounds);
			}
		} else {
			// Calcualte X spawn
			if (parameters.bounds.HasFlag(EnemyScriptable.eBoundType.right)) {
				spawnLocation.x = calculateHorizontalSpawn(EnemyScriptable.eBoundType.right, parameters, bounds);;
			} else if (parameters.bounds.HasFlag(EnemyScriptable.eBoundType.left)) {
				spawnLocation.x = calculateHorizontalSpawn(EnemyScriptable.eBoundType.left, parameters, bounds);;
			}
		}

		if (parameters.bounds.HasFlag(EnemyScriptable.eBoundType.top) && parameters.bounds.HasFlag(EnemyScriptable.eBoundType.bottom)) {
			bool spawnTop = randomBool();
			if (spawnTop) {
				// Spawn enemy in left side
				spawnLocation.y = calculateVerticalSpawn(EnemyScriptable.eBoundType.top, parameters, bounds);
			} else {
				// Spawn enemy in right side
				spawnLocation.y = calculateVerticalSpawn(EnemyScriptable.eBoundType.bottom, parameters, bounds);
			}
		} else {
			// Calculate Y spawn
			if (parameters.bounds.HasFlag(EnemyScriptable.eBoundType.top)) {
				spawnLocation.y = calculateVerticalSpawn(EnemyScriptable.eBoundType.top, parameters, bounds);
			} else if (parameters.bounds.HasFlag(EnemyScriptable.eBoundType.bottom)) {
				spawnLocation.y = calculateVerticalSpawn(EnemyScriptable.eBoundType.bottom, parameters, bounds);
			}
		}

		return spawnLocation;
	}

	private float calculateHorizontalSpawn(
		EnemyScriptable.eBoundType type,
		EnemyScriptable parameters,
		(Vector2 bottomLeft, Vector2 topRight)bounds
	) {
		if (type == EnemyScriptable.eBoundType.left) {
			return bounds.bottomLeft.x - Random.Range(0, parameters.spawnRadius) - parameters.spawnOffset;
		} else if (type == EnemyScriptable.eBoundType.right) {
			return bounds.topRight.x + Random.Range(0, parameters.spawnRadius) + parameters.spawnOffset;
		}

		return 0;
	}

	private float calculateVerticalSpawn(
		EnemyScriptable.eBoundType type,
		EnemyScriptable parameters,
		(Vector2 bottomLeft, Vector2 topRight)bounds
	) {
		if (type == EnemyScriptable.eBoundType.top) {
			return bounds.topRight.y + Random.Range(0, parameters.spawnRadius) + parameters.spawnOffset;
		} else if (type == EnemyScriptable.eBoundType.bottom) {
			return bounds.bottomLeft.y - Random.Range(0, parameters.spawnRadius) - parameters.spawnOffset;
		}

		return 0;
	}

	private bool randomBool() {
		return Random.Range(0.0f, 1.0f) > 0.5f;
	}

	private Quaternion calculateSpawnRotation(Vector3 spawnLocation) {
		Vector3 returnRotation = Vector3.zero;
		var bounds = CameraManager.calculateBounds();

		if (spawnLocation.x > bounds.topRight.x)returnRotation.x = 180;

		if (spawnLocation.y > bounds.topRight.y)returnRotation.z = -90;
		else if (spawnLocation.y < bounds.bottomLeft.y)returnRotation.z = 90;

		return Quaternion.Euler(returnRotation);
	}

	private Vector2 vectorFromFloat(float value) {
		return new Vector2(value, value);
	}

	private void OnDrawGizmos() {
		Gizmos.color = new Color(0, 1, 0.2f, 0.2f);
		(Vector2 bottomLeft, Vector2 topRight)bounds = CameraManager.calculateBounds();
		float centerY = (bounds.bottomLeft.y + bounds.topRight.y) / 2;
		float centerX = (bounds.bottomLeft.x + bounds.topRight.x) / 2;
		float boundsWidth = bounds.topRight.x - bounds.bottomLeft.x;
		float boundsHeight = bounds.topRight.y - bounds.bottomLeft.y;

		enemySpawns.ForEach((parameter) => {
			if (parameter.showBounds) {
				if (parameter.bounds.HasFlag(EnemyScriptable.eBoundType.left)) {
					Gizmos.DrawCube(
						new Vector2(bounds.bottomLeft.x - parameter.spawnRadius / 2 - parameter.spawnOffset, centerY),
						new Vector3(parameter.spawnRadius, boundsHeight, 1));
				}
				if (parameter.bounds.HasFlag(EnemyScriptable.eBoundType.right)) {
					Gizmos.DrawCube(
						new Vector2(bounds.topRight.x + parameter.spawnRadius / 2 + parameter.spawnOffset, centerY),
						new Vector3(parameter.spawnRadius, boundsHeight, 1));
				}
				if (parameter.bounds.HasFlag(EnemyScriptable.eBoundType.top)) {
					Gizmos.DrawCube(
						new Vector2(centerX, bounds.topRight.y + parameter.spawnRadius / 2 + parameter.spawnOffset),
						new Vector3(boundsWidth, parameter.spawnRadius, 1));
				}
				if (parameter.bounds.HasFlag(EnemyScriptable.eBoundType.bottom)) {
					Gizmos.DrawCube(
						new Vector2(centerX, bounds.bottomLeft.y - parameter.spawnRadius / 2 - parameter.spawnOffset),
						new Vector3(boundsWidth, parameter.spawnRadius, 1));
				}
			}
		});
	}

}