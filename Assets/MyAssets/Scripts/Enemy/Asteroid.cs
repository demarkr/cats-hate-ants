using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

public class Asteroid : Entity {
    [Title("Asteroid")]
    [ProgressBar(1, 10), SerializeField, GUIColor(0.5f, 0.5f, 1)]
    private float moveSpeed = 2;
    [ProgressBar(0, 10), SerializeField, Space(5)]
    private float directionRandomization = 2;

    private Cat catInstance;
    new private Rigidbody rigidbody;
    private void Awake() {
        rigidbody = GetComponent<Rigidbody>();
    }

    protected override void Start() {
        base.Start();
        catInstance = Cat.GetInstance();
        AddDirectionalForce(calculateMovingDirection());
    }

    public void AddDirectionalForce(Vector2 direction) {
        rigidbody.AddForce(direction.normalized * moveSpeed, ForceMode.Impulse);
    }

    Vector2 calculateMovingDirection() {
        Vector3 directionToCat = catInstance.transform.position - transform.position;
        return directionToCat * Random.Range(1, 1 + directionRandomization);
    }
}