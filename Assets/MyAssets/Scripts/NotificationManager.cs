using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using NaughtyAttributes;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class NotificationManager : MonoBehaviour {
    [System.Serializable]
    public struct NotificationArgs {
        [TextArea]
        [LabelWidth(100)]
        public string content;
        [HorizontalGroup(0.5f)]
        [Range(0, 999)]
        [LabelWidth(100)]
        public float duration;
        [HorizontalGroup]
        [LabelWidth(100)]
        [GUIColor(0, 1, 0)]
        public AudioCollectionScriptable audioCollection;

        public NotificationArgs(string content, float duration = 4, AudioCollectionScriptable audioCollection = null) {
            this.content = content;
            this.duration = duration;
            this.audioCollection = audioCollection;
        }
    }
    public Transform panel;
    public TextMeshProUGUI text;

    public float moveDuration = 0.5f;
    public float panelYOffset = 1.8f;

    private float newPanelY;
    private float panelStartY;
    private RectTransform _rectTransform;
    private static NotificationManager instance;

    // Events
    public delegate void notificationDoneDelegate();
    public static event notificationDoneDelegate onNotificationDone;
    private void Awake() {
        _rectTransform = panel.GetComponent<RectTransform>();
        instance = this;
        resetPanel();
    }

    private void Update() {
        _rectTransform.anchoredPosition = new Vector2(0, newPanelY);
    }

    public static NotificationManager GetInstance() {
        return instance;
    }

    private Tweener notificationTween;
    public void ShowNotification(NotificationArgs args) {
        if (string.IsNullOrEmpty(args.content))return;
        if (notificationTween != null)notificationTween.Kill();

        resetPanel();
        text.text = args.content;

        SoundManager.GetInstance().PlaySound(args.audioCollection);
        notificationTween = DOTween.To(() => newPanelY, (x) => newPanelY = x, -_rectTransform.rect.height / panelYOffset, moveDuration).SetEase(Ease.OutBack)
            .OnComplete(() =>
                DOTween.To(() => newPanelY, (x) => newPanelY = x, panelStartY, moveDuration)
                .SetEase(Ease.InBack).SetDelay(args.duration).SetId(5)
                .OnStart(() => onNotificationDone?.Invoke())
            );
    }

    public void EndNotification() {
        DOTween.Complete(5, true);
    }

    private void resetPanel() {
        Canvas.ForceUpdateCanvases();
        panelStartY = _rectTransform.rect.height / panelYOffset;
        newPanelY = panelStartY;
    }
}