using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Audio;

[CreateAssetMenu(menuName = "New Audio Collection", fileName = "New Audio Collection")]
public class AudioCollectionScriptable : ScriptableObject {
    [HorizontalGroup("Split", 0.5f)]
    [BoxGroup("Split/Clips"), InlineProperty]
    [AssetSelector(Paths = "Assets/MyAssets/Sounds")]
    public List<AudioClip> clips = new List<AudioClip>();
    [BoxGroup("Split/Settings"), LabelWidth(100)]
    public bool randomPitch = false;
    [BoxGroup("Split/Settings"), LabelWidth(100)]
    public bool randomVolume = false;
    [BoxGroup("Split/Settings"), ProgressBar(0,1,0), LabelWidth(100)]
    public float volume = 1;

    [Title("")]
    public AudioMixerGroup mixerGroup = default(AudioMixerGroup);
}