using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[CreateAssetMenu(fileName = "New Shop Item", menuName = "Shop/New Shop Item")]
[InlineEditor(InlineEditorModes.FullEditor)]
public class ShopItemScriptable : MonoBehaviour {
    [HorizontalGroup("Split", 0.25f)]
    [BoxGroup("Split/Right",false), PreviewField(128), HideLabel, LabelWidth(0)]
    public Sprite icon;
    [BoxGroup("Split/Left"), LabelWidth(70)]
    public new string name = "New Item";
    [BoxGroup("Split/Left"), LabelWidth(70), GUIColor(0,1,0.5f)]
    public int price = 100;
    [BoxGroup("Split/Left"), LabelWidth(120)]
    public bool singlePurchase = true;
    [BoxGroup("Split/Left"), LabelWidth(120), HideIf("singlePurchase")]
    public int maxPurchaseTimes = 1;
}