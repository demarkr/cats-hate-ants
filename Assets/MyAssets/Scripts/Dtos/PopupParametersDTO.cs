using Sirenix.OdinInspector;
using UnityEngine;

[System.Serializable]
public class PopupParametersDTO {
    private string Text;
    [SerializeField]
    private Vector3 Position;
    [SerializeField]
    private Vector3 Scale = new Vector3(0.1f, 0.1f, 0.1f);
    [SerializeField, Range(0, 10)]
    private float Duration = 0;
    [SerializeField, Range(0, 5)]
    private float moveSpeed = 0;

    public PopupParametersDTO(
        string text = "",
        Vector3 position = default(Vector3),
        Vector3 scale = default(Vector3),
        float duration = 0) => (Text, Position, Scale, Duration) = (text, position, scale, duration);
    public string GetText() {
        return Text;
    }
    public void SetText(string text) {
        Text = text;
    }

    public Vector3 GetPosition() {
        return Position;
    }
    public void SetPosition(Vector3 value) {
        Position = value;
    }

    public Vector3 GetScale() {
        return Scale;
    }

    public float GetDuration() {
        return Duration;
    }

    public float GetSpeed() {
        return moveSpeed;
    }

    [Button("Get Defaults", ButtonSizes.Medium, ButtonStyle.CompactBox)]
    private void getDefaults() {
        Score scorePrefab = Resources.Load<Score>("Score");
        this.Scale = scorePrefab.transform.localScale;
        this.Duration = scorePrefab.scoreDuration;
        this.moveSpeed = scorePrefab.moveSpeed;
    }
}