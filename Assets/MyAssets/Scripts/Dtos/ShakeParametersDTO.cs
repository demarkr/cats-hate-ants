using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;

[System.Serializable, InlineProperty()]
public class ShakeParametersDTO {
    [PropertyRange(0, 10), GUIColor(1, 1, 0), SuffixLabel("sec"), LabelWidth(100)]
    public float duration = 1;
    [PropertyRange(0, 30), GUIColor(0, 1, 1), LabelWidth(100)]
    public float strength = 1;
    [PropertyRange(0, 50), GUIColor(1, 0, 1), LabelWidth(100)]
    public int vibrato = 5;

    public ShakeParametersDTO(float duration = 1, float strength = 1, int vibrato = 10)
    {
        this.duration = duration;
        this.strength = strength;
        this.vibrato = vibrato;
    }
}