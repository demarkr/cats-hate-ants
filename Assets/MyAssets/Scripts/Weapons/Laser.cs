using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(DealDamage))]
public class Laser : MonoBehaviour {
    [SerializeField, BoxGroup("Sounds")]
    private AudioCollectionScriptable laserStartSound;
    [SerializeField, BoxGroup("Sounds")]
    private AudioCollectionScriptable laserActivatedSound;

    [SerializeField]
    private Gradient startingColor;
    [SerializeField]
    private Gradient activatedColor;
    [SerializeField]
    private float waitToActivateTime = 1.5f;
    [SerializeField]
    private float powerUpTime = 1;
    [SerializeField]
    private float activeTime = 1;
    [SerializeField]
    private UnityEvent OnActivated;
    [SerializeField]
    private UnityEvent OnDeactivated;

    private LineRenderer _lineRenderer;
    private DealDamage _dealDamage;
    private float _deltaTime;
    private float completedTime;
    private bool completed = false;
    private float alpha = 0.4f;
    private BoxCollider _collider;
    private void Awake() {
        _dealDamage = GetComponent<DealDamage>();
        _lineRenderer = GetComponent<LineRenderer>();
        _collider = GetComponent<BoxCollider>();
        _collider.enabled = false;
        setUpLaser();
        initiateLaserAnimation();
    }

    private void Start() {
        SoundManager.GetInstance().PlaySound(laserStartSound);
    }

    private void initiateLaserAnimation() {
        DOTween.To(
                () => _lineRenderer.GetPosition(1).x,
                x => _lineRenderer.SetPosition(1, new Vector3(x, 0, 0)),
                endValue : 12, duration : 1f).SetEase(Ease.OutQuint)
            .OnComplete(() => {
                completedTime = _deltaTime;
                completed = true;
                SoundManager.GetInstance().PlaySound(laserActivatedSound);
            });
    }

    private void setUpLaser() {
        _lineRenderer.colorGradient = startingColor;
        _lineRenderer.SetPosition(1, _lineRenderer.GetPosition(0));
    }

    private Color2 getGradientColors(Gradient input) {
        return new Color2(input.colorKeys[0].color, input.colorKeys[input.colorKeys.Length - 1].color);
    }

    Gradient newGradient = new Gradient();


	private void setAlpha(LineRenderer renderer, float alpha) {
        GradientAlphaKey[] alphaKeys = new GradientAlphaKey[] { new GradientAlphaKey(alpha, 0), new GradientAlphaKey(alpha, 1) };
        newGradient.SetKeys(renderer.colorGradient.colorKeys, alphaKeys);
        renderer.colorGradient = newGradient;
    }

    void Update() {
        _deltaTime += Time.deltaTime;
        setAlpha(_lineRenderer, alpha);
        if (_deltaTime > completedTime + waitToActivateTime && completed) {
            completed = false;
            activateLaser();
        }
    }

    private void activateLaser() {
        OnActivated.Invoke();
        _dealDamage.SetDoDamage(true);
        _collider.enabled = true;

        Sequence seq = DOTween.Sequence();
        seq.Append(
            _lineRenderer.DOColor(
                getGradientColors(_lineRenderer.colorGradient),
                getGradientColors(activatedColor), duration : powerUpTime)
        );
        seq.Join(
            DOTween.To(() => alpha, (x) => alpha = x, 1, powerUpTime)
        );

        seq.OnComplete(() => StartCoroutine(deactivateLaser()));
    }

    IEnumerator deactivateLaser() {
        yield return new WaitForSeconds(activeTime);
        DOTween.To(() => alpha, (x) => alpha = x, 0, powerUpTime).OnComplete(() => {
            Destroy(transform.parent.gameObject);
            OnDeactivated.Invoke();
        });
    }

    private float getXPos() {
        return _lineRenderer.GetPosition(1).x;
    }
}