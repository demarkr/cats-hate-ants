using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using HighlightPlus;
using NaughtyAttributes;
using UnityEngine;

public class CatWeapons : MonoBehaviour {

    public List<WeaponScriptable> weapons = new List<WeaponScriptable>();
    public AudioSource holdAudioSource;

    [SerializeField]
    private WeaponScriptable equippedWeapon;

    [SerializeField, Required]
    private Draggable draggable;

    [Foldout("Hold"), SerializeField]
    private float holdChargeDuration = 3;
    private float holdChargeDownDuration = 0.4f;
    [Foldout("Hold"), SerializeField]
    private float catShakeMultiplier = 2;
    [Foldout("Hold"), SerializeField]
    private float catShakeTimescale = 5;
    [Foldout("Hold"), SerializeField]
    private HighlightEffect catHighlight;

    private bool holding;
    private float holdProgress = 0;

    private Tweener holdProgressTween;
    private Tweener holdChargeTween;

    public delegate void shotWeaponDelegate();
    public static event shotWeaponDelegate onShotWeapon;

    private Cat catInstance;
    private void Start() {
        catInstance = Cat.GetInstance();

        catHighlight.fadeInDuration = holdChargeDuration;
        catHighlight.fadeOutDuration = holdChargeDownDuration;
    }

    private void Update() {
        catInstance.setShakeMultiplier(holdProgress * catShakeMultiplier);
        catInstance.setShakeTimescale(holdProgress * catShakeTimescale);

        if (Input.anyKeyDown) {
            WeaponScriptable activatedWeapon = weapons.Find(weapon => weapon.fireButton != "" && Input.GetButton(weapon.fireButton));
            if (activatedWeapon != null) {
                shootWeapon(activatedWeapon);
            }
        }
    }

    private void shootWeapon(WeaponScriptable weapon) {
        if (!hasAmmo())return;
        // Effects
        CameraManager.GetInstance().screenShake(weapon.screenShakeStrength, weapon.screenShakeDuration, weapon.screenShakeVibrato);
        SoundManager.GetInstance().PlaySound(weapon.audioCollection);

        onShotWeapon?.Invoke();

        switch (weapon.type) {
            case WeaponScriptable.eWeaponTypes.Slap:
                //TODO : ADD SLAP?
                break;
            case WeaponScriptable.eWeaponTypes.SpawnPrefab:
                Instantiate(weapon.prefab, transform.position + new Vector3(0, 0, -1), Quaternion.identity);

                // Popup pat cost, above the cat
                PatManager.instance.removePat(
                    weapon.patCost,
                    new PopupParametersDTO(position: CameraManager.GetInstance().GetWorldMousePosition() + new Vector3(0, 1))
                );
                break;
        }
    }

    public void useEquippedWeapon() {
        if (!equippedWeapon)return;
        shootWeapon(equippedWeapon);
    }

    public void equipWeapon(WeaponScriptable weapon){
        equippedWeapon = weapon;
    }

    public bool hasAmmo() {
        if(!equippedWeapon)return false;
        return PatManager.instance.getPatCount() > equippedWeapon.patCost;
    }

    private void DoneHolding() {
        if (holdProgress < 1)return;
        useEquippedWeapon();
        endHold();
        catHighlight.HitFX();
    }

    public void beginHold() {
        if (!hasAmmo())return;

        holdAudioSource.Play();
        holdProgressTween?.Kill();
        holdProgressTween = DOTween.To(() => holdProgress, (x) => holdProgress = x, 1, holdChargeDuration).OnComplete(DoneHolding);
        catHighlight.SetHighlighted(true);
    }

    public void endHold() {
        holdAudioSource.Stop();
        holdProgressTween?.Kill();
        holdProgress = 0;
        catHighlight.SetHighlighted(false);
    }

}