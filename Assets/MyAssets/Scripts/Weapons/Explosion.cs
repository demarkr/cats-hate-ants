using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using UnityEngine;

public class Explosion : MonoBehaviour {
    [SerializeField]
    private float radius = 2;
    [SerializeField]
    private float force = 2;
    [SerializeField]
    private LayerMask affectedMask;
    [SerializeField]
    private bool explodeOnStart = true;
    [SerializeField, ShowIf ("explodeOnStart")]
    private ParticleSystem particle;

    private bool exploded = false;

    private void Start () {
        exploded=false;

        if (explodeOnStart) {
            particle.Play (withChildren: true);
            Explode ();
        }
    }

    public void Explode () {
        if(exploded)return;

        List<Boid> boidsInRange = GetBoids();
        boidsInRange.ForEach(entity=>{
            float distance = Vector2.Distance((Vector2)entity.transform.position, (Vector2)transform.position);
            entity.AddForce((((Vector2)entity.transform.position - (Vector2)transform.position) * force) / distance);
            entity.GetComponent<EntityBounds>()?.setBoundsConstraint(true);
        });
        exploded=true;
    }

    private List<Boid> GetBoids(){
        List<Boid> boidInRange = new List<Boid>();
        foreach (var collider in Physics.OverlapSphere(transform.position,radius, affectedMask))
        {
            Boid _boid = collider.GetComponent<Boid>();
            if(_boid) boidInRange.Add(_boid);
        }
        return boidInRange;
    }

    private void OnDrawGizmosSelected () {
        Gizmos.color = new Color (0, 0.5f, 0.5f, 0.2f);
        Gizmos.DrawSphere (transform.position, radius*transform.localScale.x);
    }
}