using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using UnityEngine;

[CreateAssetMenu(fileName = "NewWeapon", menuName = "New Weapon")]
public class WeaponScriptable : ScriptableObject {
    public enum eWeaponTypes {
        Slap,
        SpawnPrefab
    }

    public AudioCollectionScriptable audioCollection;

    [InputAxis]
    public string fireButton;
    public int patCost = 5;
    public eWeaponTypes type;
    [ShowIf ("type", eWeaponTypes.SpawnPrefab)]
    public GameObject prefab;

    [Foldout("Screen Shake")]
    public float screenShakeStrength = 3;
    public float screenShakeDuration = 0.1f;
    public int screenShakeVibrato = 2;
}