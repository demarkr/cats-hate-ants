using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using FlyingWormConsole3;
using Sirenix.OdinInspector;
using UnityEngine;

public class SineTransformer : MonoBehaviour {
	public enum eType {
		Position,
		Rotation,
		Scale
	}

	[System.Serializable]
	public class transformDTO {
		[EnumToggleButtons]
		public eType type;
		public Vector3 amount;
		public float sineSpeed = 1;
		public bool randomOffset = false;
		[HideIf ("randomOffset")]
		public float timeOffset = 0;

		[HideInInspector]
		public (Vector3 position, Vector3 rotation, Vector3 size, Vector3 previousSine) previousTransformation;
	}

	[SerializeField]
	private List<transformDTO> options = new List<transformDTO> ();
	[SerializeField]
	private bool doTransform = true;

	private float _deltaTime;

	private void Start () {
		optionsSetup ();
	}

	private void Update () {
		if (!doTransform) return;
		_deltaTime += Time.deltaTime;
		foreach (transformDTO option in options) {
			Vector3 sineVector = sineToVector (option.sineSpeed, option.amount, _deltaTime + option.timeOffset);
			Vector3 previousSine = option.previousTransformation.previousSine;
			option.previousTransformation = (
				transform.localPosition - previousSine,
				transform.localRotation.eulerAngles - previousSine,
				transform.localScale - previousSine,
				sineVector
			);
			switch (option.type) {
				case eType.Position:
					transform.localPosition = option.previousTransformation.position + sineVector;
					break;
				case eType.Rotation:
					transform.localRotation =
						Quaternion.Euler (option.previousTransformation.rotation + sineVector);
					break;
				case eType.Scale:
					transform.localScale = option.previousTransformation.size + sineVector;
					break;
				default:
					break;
			}
		}

	}

	public void setTransformEnableState (bool state) {
		doTransform = state;
		if(!state){
			resetTransformations();
		}
	}

	public void resetTransformations()
	{
		options.ForEach((option)=>{
			transform.localPosition = option.previousTransformation.position;
			transform.localRotation = Quaternion.Euler(option.previousTransformation.rotation);
			transform.localScale = option.previousTransformation.size;
		});
	}

	private Vector3 sineToVector (float speed, Vector3 multiplier, float time) {
		float sine = Mathf.Sin (time * speed);
		return Vector3.Scale (vectorFromFloat (sine), multiplier);
	}

	private Vector3 vectorFromFloat (float value) {
		return new Vector3 (value, value, value);
	}

	private void optionsSetup () {
		options.ForEach((option)=>{
			option.previousTransformation.position = transform.localPosition;
			option.previousTransformation.rotation = transform.localRotation.eulerAngles;
			option.previousTransformation.size = transform.localScale;
		});
		options
			.FindAll ((option) => option.randomOffset)
			.ForEach ((option) => option.timeOffset = Random.Range (0.0f, 100000.0f));
	}

}