using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using Sirenix.OdinInspector;

[RequireComponent(typeof(Rigidbody))]
public class ParticlePop : MonoBehaviour {
    [HorizontalGroup("Stats")]
    [VerticalGroup("Stats/Start")]
    [LabelWidth(80)]
    public float startForce = 2;
    [VerticalGroup("Stats/Start")]
    [LabelWidth(110)]
    [Range(0,2)]
    public float startWaitDuration = 0.3f;

    [VerticalGroup("Stats/Movement")]
    [LabelWidth(110)]
    public Ease goEase = Ease.InOutQuint;
    [VerticalGroup("Stats/Movement")]
    [LabelWidth(110)]
    [Range(0,5)]
    public float duration = 1;

    private Rigidbody _rb;
    private Vector3 goal;
    private void Awake() {
        _rb = GetComponent<Rigidbody>();
    }

    private void calculateGoal() {
        var bounds = CameraManager.calculateBounds();
        goal = bounds.bottomLeft;
    }

    private void Start() {
        _rb.velocity = new Vector2(Helpers.RandomBool() ? 0.8f : -0.8f, 1) * startForce;
        StartCoroutine(StartGoing());
    }

    private IEnumerator StartGoing() {
        yield return new WaitForSeconds(startWaitDuration);
        calculateGoal();
        _rb.isKinematic = true;
        transform.DOMove(goal, duration).SetEase(goEase).OnComplete(() => Destroy(gameObject));
    }
}