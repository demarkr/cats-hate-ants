using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundPlayer : MonoBehaviour {
    [SerializeField, Range(0, 1)]
    private float soundPlayProbability = 0f;
    [SerializeField]
    private AudioCollectionScriptable audioCollection;
    [SerializeField]
    private bool playOnStart = false;

    private float probabilityOnStart;


    private void Start() {
        probabilityOnStart = soundPlayProbability;
        if(playOnStart){
            PlaySound();
        }
    }

    private void FixedUpdate() {
        if (soundPlayProbability == 0)return;
        if (Random.Range(0.0f, 1.0f) < soundPlayProbability) {
            PlaySound();
        }
    }

    public void PlaySound() {
        SoundManager.GetInstance().PlaySound(audioCollection);
    }

    public void setSoundProbablity(float value) {
        soundPlayProbability = value;
    }

    public void resetSoundProbablity() {
        soundPlayProbability = probabilityOnStart;
    }
}