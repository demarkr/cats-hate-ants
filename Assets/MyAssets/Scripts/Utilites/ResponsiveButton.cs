using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent (typeof (SineTransformer))]
public class ResponsiveButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler, IPointerUpHandler {
    [FoldoutGroup ("Mouse Over"), SerializeField]
    private float enter_growSize = 0.1f;
    [FoldoutGroup ("Mouse Over"), SerializeField]
    private float enter_growSpeed = 0.4f;

    [FoldoutGroup ("Mouse Exit"), SerializeField]
    private float exit_shrinkSpeed = 0.2f;

    [FoldoutGroup ("Mouse Down"), SerializeField]
    private Vector3 down_shakeStrength = new Vector3 (0.1f, 0.1f, 0.1f);
    [FoldoutGroup ("Mouse Down"), SerializeField]
    private float down_shakeDuration = 0.4f;
    [FoldoutGroup ("Mouse Down"), SerializeField]
    private int down_shakeVibrato = 3;
    [FoldoutGroup ("Mouse Down"), SerializeField]
    private Vector3 down_growSize = new Vector3 (0.2f, 0.2f, 0.2f);
    [FoldoutGroup ("Mouse Down"), SerializeField]
    private float down_growSpeed = 0.1f;

    (Vector3 position, Quaternion rotation, Vector3 scale) startSettings;
    SineTransformer _sineTransformer;

    private void Awake () {
        _sineTransformer = GetComponent<SineTransformer> ();
    }

    private void Start () {
        startSettings = (
            transform.position,
            transform.rotation,
            transform.localScale
        );
    }

    public void OnPointerEnter (PointerEventData eventData) {
        DOTween.Restart(99);
        DOTween.Kill(99);
        transform.DOScale (startSettings.scale.x + enter_growSize, enter_growSpeed).SetEase(Ease.OutElastic).SetId(99);
        _sineTransformer.setTransformEnableState (true);
    }

    public void OnPointerExit (PointerEventData eventData) {
        DOTween.Restart(98);
        DOTween.Kill(98);
        transform.DOScale (startSettings.scale.x, exit_shrinkSpeed).SetEase(Ease.OutElastic).SetId(98);
        _sineTransformer.setTransformEnableState (false);
        _sineTransformer.resetTransformations();
    }

    public void OnPointerDown (PointerEventData eventData) {
        DOTween.Restart(97);
        DOTween.Kill(97);
        transform.DOPunchPosition (down_shakeStrength, down_shakeDuration, down_shakeVibrato).SetId(97);
        transform.DOPunchScale (down_growSize, down_growSpeed).SetId(0);
    }

    public void OnPointerUp (PointerEventData eventData) {
        DOTween.Restart(96);
        DOTween.Kill(96);
        transform.DOPunchPosition (-down_shakeStrength, down_shakeDuration / 1.5f, down_shakeVibrato).SetId(96);
    }
}