using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

public class MouseOver : MonoBehaviour {
    [SerializeField, ReadOnly]
    private bool isMouseOver = false;

    void OnMouseExit () {
        isMouseOver = false;
    }

    void OnMouseEnter () {
        isMouseOver = true;
    }

    public bool MouseOverState(){
        return isMouseOver;
    }
}