using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
#if UNITY_EDITOR
using Sirenix.Utilities.Editor;
#endif
[CreateAssetMenu(fileName = "New Game Progress", menuName = "New Game Progress")]
public class GameProgressScriptable : ScriptableObject {
    [System.Serializable]
    public enum eGameAction {
        AddFood,
        ChangeEnemyParameters
    }

    [System.Serializable]
    public enum eEnemyAction {
        SpawnState,
        MinSpawnFreq,
        SpawnFreq,
        Bounds,
        GroupSpawnAmount,
        ResetEnemy
    }

    [System.Serializable, InlineProperty, HideLabel]
    public class EnemyParameters {
        [HorizontalGroup("Split", 0.8f)]
        [Title("@this.enemy?.name", titleAlignment : TitleAlignments.Centered)]
        [InlineEditor(InlineEditorModes.FullEditor), OnInspectorGUI("@this.enemyPrefabPreview = this.enemy?.prefab")]
        public EnemyScriptable enemy;

        [HorizontalGroup("Split")]
        [SerializeField, ShowIf("@enemy!=null"), ReadOnly, PreviewField(70, Sirenix.OdinInspector.ObjectFieldAlignment.Left), HideLabel]
        private GameObject enemyPrefabPreview;

        [EnumToggleButtons]
        [LabelWidth(90), HideLabel]
        public eEnemyAction action;

        [ShowIf("@this.action==eEnemyAction.SpawnState")]
        public bool spawnState;

        [ShowIf("@this.action==eEnemyAction.MinSpawnFreq")]
        [InlineButton("@this.minSpawnFreq = this.enemy.minFrequency", "GET")]
        public float minSpawnFreq;

        [ShowIf("@this.action==eEnemyAction.SpawnFreq")]
        [InlineButton("@this.spawnFreq = this.enemy.spawnFrequency", "GET")]
        [MinMaxSlider(0, 8)]
        public Vector2 spawnFreq;

        [ShowIf("@this.action==eEnemyAction.Bounds")]
        [InlineButton("@this.bounds = this.enemy.bounds", "GET")]
        public EnemyScriptable.eBoundType bounds;

        [ShowIf("@this.action==eEnemyAction.GroupSpawnAmount")]
        [InlineButton("@this.groupSpawnAmount = this.enemy.groupSpawnAmount", "GET")]
        public int groupSpawnAmount;
    }

    [System.Serializable]
    public class Action {
        [HorizontalGroup(0.25f)]
        [LabelWidth(80)]
        [HideLabel]
        [EnumToggleButtons]
        public eGameAction gameAction;

        [HorizontalGroup]
        [ShowIf("@(this.gameAction == eGameAction.AddFood)")]
        [AssetsOnly]
        [PreviewField(75)]
        [LabelText("@this.foodEntity?.name")]
        public GameObject foodEntity;

        [HorizontalGroup]
        [ShowIf("@(this.gameAction == eGameAction.ChangeEnemyParameters)")]
        public EnemyParameters enemyParameters;
    }

    [System.Serializable]
    public class GameProgress {
        [GUIColor(1f, 0, 0.5f)]
        [Title("@this.Level + \" -- LEVEL\"", titleAlignment : TitleAlignments.Centered)]
        [PropertyRange(0, "@XPManager.MAX_LEVEL")]
        [LabelWidth(100)]
        public int Level;

        public List<Action> actions = new List<Action>();

        public NotificationManager.NotificationArgs notification;

        [HideInInspector]
        public bool actionCalled;
    }

    [ListDrawerSettings(NumberOfItemsPerPage = 1, OnTitleBarGUI = "drawSortButton")]
    public List<GameProgress> gameProgresses = new List<GameProgress>();

    private void drawSortButton() {
#if UNITY_EDITOR
        if (SirenixEditorGUI.ToolbarButton(EditorIcons.Tree)) {
            sortList();
        }
#endif
    }

    private void sortList() {
        gameProgresses.Sort((x, y) => x.Level.CompareTo(y.Level));
    }
}