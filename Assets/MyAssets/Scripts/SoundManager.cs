using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Audio;

public class SoundManager : MonoBehaviour {
    [SerializeField, AssetsOnly]
    private GameObject SoundObject;

    [SerializeField, MinMaxSlider(-1, 1, true)]
    private Vector2 volumeRange;
    [SerializeField, MinMaxSlider(0.5f, 1.5f, true)]
    private Vector2 pitchRange;

    private static SoundManager instance;
    private void Awake() {
        instance = this;
    }

    public static SoundManager GetInstance() {
        return instance;
    }

    public void PlaySound(AudioCollectionScriptable audioCollection) {
        if (audioCollection == null)return;

        AudioSource ads = Instantiate(SoundObject).GetComponent<AudioSource>();

        ads.clip = audioCollection.clips[Random.Range(0, audioCollection.clips.Count - 1)];
        if (audioCollection.randomPitch)ads.pitch = Random.Range(pitchRange.x, pitchRange.y);
        if (audioCollection.randomVolume)ads.volume = audioCollection.volume + Random.Range(volumeRange.x, volumeRange.y);
        if (audioCollection.mixerGroup != default(AudioMixerGroup))ads.outputAudioMixerGroup = audioCollection.mixerGroup;
        ads.Play();

        Destroy(ads.gameObject, ads.clip.length);
    }
}