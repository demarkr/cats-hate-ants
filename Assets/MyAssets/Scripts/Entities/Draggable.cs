using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

public class Draggable : MonoBehaviour {
	[FoldoutGroup("Drag"), SerializeField]
	private bool canDrag = true;
	[FoldoutGroup("Drag"), SerializeField]
	private float dragStartTime = 0.5f;
	[FoldoutGroup("Drag"), SerializeField]
	private float dragFollowSpeed = 3f;
	[FoldoutGroup("Drag"), SerializeField]
	private UnityEvent OnDrag;
	[FoldoutGroup("Drag"), SerializeField]
	private UnityEvent OnEndDrag;
	[FoldoutGroup("Drag"), SerializeField]
	private Rigidbody rb;

	[SerializeField]
	private UnityEvent mouseDownEvent;

	[FoldoutGroup("Hold"), SerializeField]
	private UnityEvent onHold;
	[FoldoutGroup("Hold"), SerializeField]
	private UnityEvent onEndHold;
	[FoldoutGroup("Hold"), SerializeField]
	private float holdTime = 1;
	[FoldoutGroup("Hold"), SerializeField, InfoBox("The radius at which dragging becomes holding")]
	private float holdDistanceThreshold = 0.1f;

	private bool drag;
	private float mouseDownTime;
	private float deltaTime;
	private bool dragging = false;
	private bool holding = false;

	private void Update() {
		deltaTime += Time.deltaTime;
		if (!canDrag)return;

		dragging = deltaTime > mouseDownTime + dragStartTime && drag;
		if (dragging) {
			OnDrag?.Invoke();
			float distance = Vector2.Distance((Vector2)CameraManager.GetInstance().GetWorldMousePosition(), (Vector2)transform.position);
			rb.velocity =
				(((Vector2)CameraManager.GetInstance().GetWorldMousePosition() - (Vector2)transform.position).normalized * dragFollowSpeed) * distance;

			if (distance <= holdDistanceThreshold && !holding) {
				onHold?.Invoke();
				holding = true;
			} else if (distance > holdDistanceThreshold && holding) {
				onEndHold?.Invoke();
				holding = false;
			}

		} else {
			rb.velocity = Vector2.zero;
			OnEndDrag?.Invoke();
			if (holding) {
				onEndHold?.Invoke();
				holding = false;
			}
		}
	}

	private Vector3 vectorSetZ(Vector3 value, float zValue) {
		return new Vector3(value.x, value.y, zValue);
	}

	private void OnMouseUp() {
		drag = false;
	}

	void OnMouseDown() {
		if (rb)rb.velocity = Vector2.zero;
		mouseDownTime = deltaTime;
		drag = true;
		mouseDownEvent.Invoke();
	}

	public bool isDragging() {
		return dragging;
	}

	public void setCanDrag(bool state){
		canDrag = state;
	}
}