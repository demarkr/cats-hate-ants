using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

public class Clickable : MonoBehaviour {
	public UnityEvent MouseDown;
	public UnityEvent MouseUp;
	public UnityEvent MouseOver;
	public UnityEvent MouseExit;

	[FoldoutGroup("Single Click"), SerializeField]
	private bool allowSingleClick = true;
	[FoldoutGroup("Single Click"), InfoBox("If we click once in this time-frame")]
	public float singleClickTimeFrame = 0.35f;
	[FoldoutGroup("Single Click")]
	public UnityEvent SingleClick;

	private float clickTime;
	private float firstClickTime;
	private float _deltaTime;
	private Draggable _draggable;

	private void Awake() {
		_draggable = GetComponent<Draggable>();
	}

	private void Update() {
		_deltaTime += Time.deltaTime;

		// We clicked, and the single-click timeframe is over
		if (allowSingleClick && firstClickTime != 0 && _deltaTime > firstClickTime + singleClickTimeFrame) {
			// Clicked at least once after the first click
			if (clickTime > firstClickTime || (_draggable && _draggable.isDragging())) {
				firstClickTime = 0;
				return;
			}

			// Single click
			firstClickTime = 0;
			SingleClick?.Invoke();
		}
	}

	public void setAllowSingleClick(bool state) {
		allowSingleClick = state;
	}

	private void OnMouseDown() {
		if (firstClickTime == 0)
			firstClickTime = _deltaTime;

		clickTime = _deltaTime;
		MouseDown?.Invoke();
	}

	private void OnMouseUp() {
		MouseUp?.Invoke();
	}

	private void OnMouseOver() {
		MouseOver?.Invoke();
	}

	private void OnMouseExit() {
		MouseExit?.Invoke();
	}
}