using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

public class Slapable : MonoBehaviour {

	public enum eSlapAction {
		Knockback,
		Kill,
		Damage
	}

	[SerializeField, BoxGroup("Pats")]
	private int slapCost = 5;
	[SerializeField, BoxGroup("Pats")]
	private Vector2 slapRewardRange = Vector2.one;
	[SerializeField, BoxGroup("Pats"), ShowIf("@(slapCost+slapRewardRange.magnitude)>0")]
	private PopupParametersDTO slapPopupArgs;

	[SerializeField, BoxGroup("Slap Action")]
	private eSlapAction slapAction;
	[SerializeField, BoxGroup("Slap Action"), ShowIf("@slapAction == eSlapAction.Damage"), PropertyRange(0, "@GetComponent<Entity>().getHealthPoints()")]
	private int slapDamage = 1;

	[Space(20), SerializeField, Range(0, 100)]
	private float slapStrength = 5;

	public UnityEvent OnSlap;

	public AudioCollectionScriptable slapSound;

	[LabelWidth(100)]
	public ShakeParametersDTO screenShake;

	protected Boid _boid;
	new private Rigidbody rigidbody;
	protected Entity _entity;

	protected virtual void Awake() {
		_boid = GetComponent<Boid>();
		_entity = GetComponent<Entity>();
		rigidbody = GetComponent<Rigidbody>();
	}

	public virtual void Slap(Vector2 forwardDirection, float slapStrength = 1, float randomMuliplier = 1.5f) {
		if (PatManager.instance.getPatCount() < slapCost)return;

		OnSlap?.Invoke();
		PatManager.instance.removePat(slapCost);
		PatManager.instance.addPat(Mathf.RoundToInt(Random.Range(slapRewardRange.x, slapRewardRange.y)), slapPopupArgs);

		if (slapRewardRange.magnitude > 0)
			SoundManager.GetInstance().PlaySound(slapSound);

		switch (slapAction) {
			case eSlapAction.Knockback:
				knockBack(forwardDirection, slapStrength, randomMuliplier);
				break;
			case eSlapAction.Kill:
				killEntity();
				break;
			case eSlapAction.Damage:
				_entity.Damage(slapDamage);
				break;
		}

		// Screen Shake
		CameraManager.GetInstance().screenShake(screenShake.strength, screenShake.duration, screenShake.vibrato);

		// Spawn particle
		ParticleManager.GetInstance().particleAtMouse(ParticleManager.eParticleType.Confetti);
	}

	public virtual void SlapAwayFromCat() {
		if (!Cat.GetInstance())return;
		Slap(Cat.GetInstance().transform.position - transform.position, slapStrength, 0);
	}

	private void knockBack(Vector2 forwardDirection, float slapStrength = 1, float randomMuliplier = 1.5f) {
		Vector2 slapDir = forwardDirection.normalized * -slapStrength;
		slapDir = new Vector2(slapDir.x * Random.Range(1, slapStrength), slapDir.y * Random.Range(1, slapStrength));
		Vector2 randomForce = new Vector2(0, Random.Range(-slapStrength * randomMuliplier, slapStrength * randomMuliplier));

		if (_boid) {
			_boid.ResetExternalForce();
			_boid.AddForce(slapDir + randomForce);
		} else if (rigidbody) {
			rigidbody.AddForce(slapDir + randomForce);
		}
	}

	private void killEntity() {
		_entity.Kill();
	}
}