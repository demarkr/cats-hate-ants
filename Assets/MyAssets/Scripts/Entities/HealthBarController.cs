using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

public class HealthBarController : MonoBehaviour {
    private Entity entityReference;

    [PropertyRange(0, 10), OnValueChanged("updatePosition"), LabelWidth(100)]
    public float yOffset;

    public void setEntityRef(Entity entity) {
        entityReference = entity;
        entity.OnKill += entityRefDied;
        transform.name = $"Health-Bar | {entity.name}";
    }

    public void UnParent() {
        transform.SetParent(null, true);
    }

    private void Update() {
        if (entityReference && transform.parent == null) {
            updatePosition();
        }
    }

    private void updatePosition() {
        if(!entityReference && !Application.isPlaying){
            entityReference = transform.parent.GetComponent<Entity>();
        }
        transform.position = entityReference.transform.position + new Vector3(0, yOffset, 0);
        transform.rotation = Quaternion.identity;
    }

    private void entityRefDied() {
        entityReference.OnKill -= entityRefDied;
        Destroy(gameObject);
    }
}