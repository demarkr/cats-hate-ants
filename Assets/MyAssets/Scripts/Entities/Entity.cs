using System;
using DG.Tweening;
using HighlightPlus;
using Sirenix.OdinInspector;
using UnityEngine;

public class Entity : MonoBehaviour {

	// Health
	[SerializeField, BoxGroup("Health"), ProgressBar(0, 20)]
	private int HealthPoints = 3;

	[BoxGroup("Health Bar")]
	[SerializeField, ShowIf("@HealthPoints > 1")]
	private Transform healthBar;

	[BoxGroup("Health Bar")]
	[SerializeField, ShowIf("@HealthPoints > 1"), Range(0, 2)]
	private float healthBar_updateDuration = 0.25f;

	// Sounds
	[SerializeField, FoldoutGroup("Sounds")]
	private AudioCollectionScriptable damageSound;
	[SerializeField, FoldoutGroup("Sounds")]
	private AudioCollectionScriptable killSound;

	[FoldoutGroup("Effects")]
	public bool changeHueSpeedOnKill = false;
	[FoldoutGroup("Effects"), ShowIf("changeHueSpeedOnKill")]
	public float hueChangeSpeedMultiplier = 2;
	[FoldoutGroup("Effects")]
	public bool highlightOnStart = false;
	[FoldoutGroup("Effects")]
	public bool hitFxOnStart = false;
	[FoldoutGroup("Effects")]
	public Vector3 punchScaleOnStart = Vector3.one;

	// Events
	public delegate void OnDamageDelegate();
	public event OnDamageDelegate OnDamage;

	public delegate void OnKillDelegate();
	public event OnKillDelegate OnKill;

	private HighlightEffect _highlightEffect;
	private int startingHealth;

	protected virtual void Start() {
		startingHealth = HealthPoints;
		_highlightEffect = GetComponent<HighlightEffect>();

		if (highlightOnStart) {
			_highlightEffect.SetHighlighted(true);
		}
		if (hitFxOnStart) {
			_highlightEffect.HitFX();
		}
		if (punchScaleOnStart != Vector3.one) {
			transform.DOPunchScale(punchScaleOnStart, 0.4f, 3);
		}

		if (healthBar)setupHealthbar();
	}

	private void setupHealthbar() {
		var healthBarController = healthBar.parent.GetComponent<HealthBarController>();
		healthBarController.setEntityRef(this);
		healthBarController.UnParent();
	}

	public virtual void Kill() {
		OnKill?.Invoke();
		SoundManager.GetInstance().PlaySound(killSound);
		if (changeHueSpeedOnKill) {
			// Change camera colors faster
			CameraManager.GetInstance().AdvanceColorMultiplier(hueChangeSpeedMultiplier);
		}
		Destroy(gameObject);
	}

	public virtual void Damage(int amount = 1) {
		HealthPoints -= amount;
		SoundManager.GetInstance().PlaySound(damageSound);

		if (healthBar) {
			float healthBarScale = Mathf.InverseLerp(0.0f, (float)startingHealth, (float)HealthPoints);
			healthBar.DOScale(Vector3.one.setXValue(healthBarScale), healthBar_updateDuration);
		}

		OnDamage?.Invoke();
		if (HealthPoints <= 0) {
			Kill();
		}
	}
	protected virtual void Update() {
		transform.position = transform.position.setZValue(1);
	}

	public virtual int getHealthPoints() {
		return HealthPoints;
	}
}