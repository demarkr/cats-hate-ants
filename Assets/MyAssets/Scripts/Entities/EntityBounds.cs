using Sirenix.OdinInspector;
using UnityEngine;

public class EntityBounds : MonoBehaviour {
	[SerializeField]
	public enum eBoundsAction {
		Block,
		Kill,
		Knockback
	}

	[SerializeField]
	private bool boundsConstraint = true;

	[SerializeField, EnumToggleButtons]
	private eBoundsAction boundsAction = eBoundsAction.Kill;

	[SerializeField]
	private Vector2 rewardRange = Vector2.one;

	[SerializeField, ShowIf("@this.rewardRange.magnitude > 0")]
	private AudioCollectionScriptable rewardSound;

	[BoxGroup("Score Showup"), SerializeField, ShowIf("@(this.rewardRange.magnitude > 0)")]
	private PopupParametersDTO scoreArgs;

	[ShowIf("@this.boundsAction == eBoundsAction.Knockback"),SerializeField]
	private float knockbackForce = 20;

	private Entity _entity;
	new private Rigidbody rigidbody;
	private Cat catInstace;

	private void Awake() {
		_entity = GetComponent<Entity>();
		rigidbody = GetComponent<Rigidbody>();
	}
	private void Start() {
		catInstace = Cat.GetInstance();
	}

	public void setBoundsConstraint(bool state) {
		boundsConstraint = state;
	}

	public void setBoundsAction(eBoundsAction action) {
		boundsAction = action;
	}

	private void Update() {
		if (!boundsConstraint)return;
		(Vector2 bottomLeft, Vector2 topRight)bounds = CameraManager.calculateBounds();

		if (transform.position.x < bounds.bottomLeft.x || transform.position.x > bounds.topRight.x ||
			transform.position.y < bounds.bottomLeft.y || transform.position.y > bounds.topRight.y) {
			outOfBounds(bounds);
		}
	}

	public virtual void outOfBounds((Vector2 bottomLeft, Vector2 topRight)bounds) {
		scoreArgs.SetPosition(transform.position);
		switch (boundsAction) {
			case eBoundsAction.Kill:
				PatManager.instance.addPat(Mathf.RoundToInt(Random.Range(rewardRange.x, rewardRange.y)), scoreArgs);
				if (rewardRange.magnitude > 0) {
					SoundManager.GetInstance().PlaySound(rewardSound);
				}

				_entity.Kill();
				break;
			case eBoundsAction.Block:
				transform.position = new Vector2(
					Mathf.Clamp(transform.position.x, bounds.bottomLeft.x, bounds.topRight.x),
					Mathf.Clamp(transform.position.y, bounds.bottomLeft.y, bounds.topRight.y)
				);
				break;
			case eBoundsAction.Knockback:
				if (catInstace) {
					rigidbody.velocity = (catInstace.transform.position - transform.position).normalized * knockbackForce;
				}
				break;

			default:
				break;
		}

		if (boundsAction == eBoundsAction.Kill) {

		} else if (boundsAction == eBoundsAction.Block) {

		}

	}
}