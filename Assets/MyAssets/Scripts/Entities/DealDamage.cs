using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using UnityEngine;
using UnityEngine.Events;

public class DealDamage : MonoBehaviour {
	[SerializeField]
	private bool DoDamage = false;

	[SerializeField]
	private int Damage = 1;

	[Tag, SerializeField]
	private string damageTag;

	public delegate void OnDealtDamage();
	public event OnDealtDamage DealtDamageEvent;

	public UnityEvent dealtDamageUnityEvent;

	void OnCollisionEnter(Collision other) {
		collision(other.transform);
	}
	void OnTriggerEnter(Collider other) {
		collision(other.transform);
	}

	private void collision(Transform other) {
		if (other.transform.tag.Equals(damageTag)) {
			dealDamage(other.transform);
		}
	}

	private void dealDamage(Transform other) {
		if (!DoDamage)return;

		Entity otherEntity = other.GetComponent<Entity>() ?? other.parent.GetComponent<Entity>();
		if (!otherEntity) { Debug.LogError($"There is not entity component on {other.gameObject.name}"); return; }
		otherEntity.Damage(Damage);
		DealtDamageEvent?.Invoke();
		dealtDamageUnityEvent?.Invoke();
	}

	public void SetDoDamage(bool state) {
		DoDamage = state;
	}
}