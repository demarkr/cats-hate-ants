using UnityEditor;
using UnityEngine;
using Sirenix.OdinInspector;
using Sirenix.OdinInspector.Editor;
using Sirenix.Utilities.Editor;

public class GameProgressEditor : OdinMenuEditorWindow
{
    [MenuItem("Tools/Game Progress Editor")]
    public static void OpenWindow(){
        GetWindow<GameProgressEditor>().Show();
    }

	protected override OdinMenuTree BuildMenuTree()
	{
		var tree = new OdinMenuTree();

        tree.AddAllAssetsAtPath("Game Progresses", "Assets/MyAssets/Scriptables/Game-Progress", typeof(GameProgressScriptable));

        return tree;
	}
}
