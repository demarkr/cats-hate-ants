using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;
using static GameProgressScriptable;

public class GameProgressManager : MonoBehaviour {

	[SerializeField]
	private GameProgressScriptable gameProgress;

	private GameProgressScriptable gameProgressRuntime;

	private void Awake() {
		if (!gameProgress)return;
		GameProgressScriptable clone = Object.Instantiate<GameProgressScriptable>(gameProgress);
		clone.name = gameProgress.name;
		clone.gameProgresses.ForEach(p => p.actionCalled = false);

		gameProgressRuntime = clone;
	}

#if UNITY_EDITOR
	[Button("Open Game Progress Manager", ButtonSizes.Large)]
	private void openWindow() {
		EditorApplication.ExecuteMenuItem("Tools/Game Progress Editor");
	}
#endif

	private void Start() {
		// ScoreKeeper.onScoreChanged += checkActions;
		XPManager.onLevelUp += checkActions;
	}

	private void OnDestroy() {
		// ScoreKeeper.onScoreChanged -= checkActions;
		XPManager.onLevelUp -= checkActions;
	}

	private void checkActions(int newLevel) {
		gameProgressRuntime.gameProgresses.FindAll(x => !x.actionCalled).ForEach(progress => {
			if (newLevel >= progress.Level) {
				progress.actions.ForEach(action => callAction(action));

				progress.actionCalled = true;
				NotificationManager.GetInstance().ShowNotification(progress.notification);
			}
		});
	}

	private void callAction(Action action) {
		switch (action.gameAction) {
			case eGameAction.AddFood:
				addFoodAction(action.foodEntity);
				break;
			case eGameAction.ChangeEnemyParameters:
				changeEnemyParams(action.enemyParameters);
				break;
		}
	}

	private void changeEnemyParams(EnemyParameters parameters) {
		switch (parameters.action) {
			case eEnemyAction.SpawnState:
				EnemySpawner.GetInstance().setSpawnState(parameters.enemy, parameters.spawnState);
				break;
			case eEnemyAction.SpawnFreq:
				EnemySpawner.GetInstance().setSpawnFreq(parameters.enemy, parameters.spawnFreq);
				break;
			case eEnemyAction.MinSpawnFreq:
				EnemySpawner.GetInstance().setMinSpawnFreq(parameters.enemy, parameters.minSpawnFreq);
				break;
			case eEnemyAction.Bounds:
				EnemySpawner.GetInstance().setBounds(parameters.enemy, parameters.bounds);
				break;
			case eEnemyAction.GroupSpawnAmount:
				EnemySpawner.GetInstance().setGroupSpawnAmount(parameters.enemy, parameters.groupSpawnAmount);
				break;
			case eEnemyAction.ResetEnemy:
				EnemySpawner.GetInstance().resetRuntimeEnemyParams(parameters.enemy);
				break;

			default:
				break;
		}
	}

	private void addFoodAction(GameObject foodPrefab) {
		// Add food
		FoodLifeManager.GetInstance().AddFood(foodPrefab);
	}

}